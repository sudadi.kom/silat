<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Setting extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

    $this->load->model('moduser');
    $this->load->model('modpelatihan');
    $menu = 1;
    if ($this->session->userdata('usrmsk') == NULL) {
      redirect('main');
    } else if ($this->session->userdata('iduser') != 1) {
      $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
      if (!in_array($menu, $aksesmenu)) {
        redirect('main');
      }
    }
  }

  public function index()
  {
    $thn =  date('Y') + 1;
    $data['banner'] = false;
    $data['page'] = 'usulanpage';
    $data['judul'] = 'Pengajuan Usulan Pelatihan';
    $data['content']['result'] = $this->modpelatihan->getusulan($thn, $this->session->userdata('idunit'), 0);

    $this->load->view('mainview', $data);
  }

  public function user()
  {
    $data['banner'] = false;
    $data['page'] = 'setuserpage';
    $data['judul'] = 'Setting Pengguna';
    $data['content']['action'] = 'setting/saveuser';
    $data['content']['result'] = $this->moduser->getuser();
    $this->load->view('mainview', $data);
  }

  public function saveuser()
  {
    if ($this->input->post()) {
      $username = $this->input->post('username');
      $realname = $this->input->post('realname');
      $password = $this->input->post('password');
      $idunit = $this->input->post('unit');
      $level = $this->input->post('level');
      $this->db->insert('tuser', array(
        'username' => $username, 'password' => sha1($password), 'nama' => $realname,
        'idunit' => $idunit, 'idsatker' => 0, 'level' => $level, 'status' => 1
      ));
      if ($this->db->affected_rows() > 0) {
        $this->session->set_flashdata('success', 'Tambah Pengguna BERHASIL.');
      } else {
        $this->session->set_flashdata('error', 'Tambah Pengguna GAGAL');
      }
    }
    redirect('setting/user');
  }

  public function hapususer($iduser)
  {
    $this->db->delete('tuser', array('iduser' => $iduser));
    if ($this->db->affected_rows() > 0) {
      $this->session->set_flashdata('success', 'Data berhasil dihapus.');
    } else {
      $this->session->set_flashdata('error', 'Data gagal dihapus');
    }
    redirect(base_url('setting/user'));
  }
  public function updstatususer($iduser, $status)
  {
    $this->db->where('iduser', $iduser);
    $this->db->update('tuser', array('status' => !$status));
    redirect(base_url('setting/user'));
  }
  public function onoff()
  {
    $data['banner'] = false;
    $data['page'] = 'onoffpage';
    $data['judul'] = 'Setting Buka/Tutup Usulan';
    $data['content']['action'] = 'setting/saveonoff';
    $data['content']['onoff'] = $this->modpelatihan->getonoff();
    $this->load->view('mainview', $data);
  }
  public function saveonoff()
  {
    if ($this->input->post()) {
      $onoff = $this->input->post('onoff');
      $this->db->replace('tonoff', array('id' => 1, 'onoff' => $onoff));
      if ($this->db->affected_rows() > 0) {
        $this->session->set_flashdata('success', 'Update data BERHASIL');
      } else {
        $this->session->set_flashdata('error', 'Update data GAGAL');
      }
    }
    redirect(base_url('setting/onoff'));
  }
  public function satker()
  {
    $this->load->model('modref');
    $data['banner'] = false;
    $data['page'] = 'satker';
    $data['judul'] = 'Setting Data Satker';
    $data['content']['dtsatker'] = $this->modref->getdatasat('1=1');
    $this->load->view('mainview', $data);
  }
  public function formsat($id = null)
  {
    if ($id) {
      $data['edit'] = true;
      $data['dtsat'] = $this->modref->getdatasat("id_unit_kerja='$id'");
    } else {
      $lastrec = $this->modref->getdatasat('true order by id_unit_kerja desc limit 1');
      $id = $lastrec[0]->id_unit_kerja + 1;
      $data['idsat'] = $id;
      $data['edit'] = false;
    }
    $data['judul'] = 'Form Satker';
    $data['action'] = 'setting/formsatsave';
    $this->load->view('formsatker', $data);
  }
  public function formsatsave()
  {
    $validation = $this->form_validation;
    $validation->set_rules([
      ['field' => 'idsat', 'label' => 'ID Satker', 'rules' => 'required'],
      ['field' => 'namasat', 'label' => 'Nama Satker', 'rules' => 'required']
    ]);

    if ($validation->run()) {
      if ($this->input->post()) {
        $proses = $this->modref->savedtsat();
        if ($proses) {
          $this->session->set_flashdata('success', 'Proses berhasil');
        } else {
          $this->session->set_flashdata('error', 'Proses GAGAL');
        }
      }
    } else {
      $this->session->set_flashdata('error', 'Data Not Valid');
    }
    redirect('setting/satker');
  }
  public function pegawai()
  {
    $data['banner'] = false;
    $data['page'] = 'pegawai';
    $data['judul'] = 'Setting Data Pegawai';
    $data['content']['action'] = 'setting/syncdatapeg';
    $data['content']['dtpegawai'] = $this->modref->getdatapeg('1=1');
    $this->load->view('mainview', $data);
  }
  public function formpeg($id = null)
  {
    if ($id) {
      $data['edit'] = true;
      $data['dtpeg'] = $this->modref->getdatapeg("id_pegawai='$id'");
    } else {
      $id = 1;
      if ($lastrec = $this->modref->getdatapeg('true order by id_pegawai desc limit 1')) {
        $id = $lastrec[0]->id_pegawai + 1;
      }
      $data['idpeg'] = $id;
      $data['edit'] = false;
    }
    $data['refstatus'] = $this->modref->getrefstatpeg('1=1');
    $data['refjenjang'] = $this->modref->getrefjenjang('1=1');
    $data['refunit'] = $this->modref->getdatasat('1=1');
    $data['judul'] = 'Form Pegawai';
    $data['action'] = 'setting/formpegsave';
    $this->load->view('formpegawai', $data);
  }
  public function formpegsave()
  {
    $validation = $this->form_validation;
    $validation->set_rules([
      ['field' => 'idpeg', 'label' => 'ID Pegawai', 'rules' => 'required'],
      ['field' => 'namapeg', 'label' => 'Nama Pegawai', 'rules' => 'required'],
      ['field' => 'nip', 'label' => 'NIP', 'rules' => 'required'],
      ['field' => 'statpeg', 'label' => 'Status Pegawai', 'rules' => 'required'],
      ['field' => 'unitker', 'label' => 'Unit Kerja', 'rules' => 'required'],
      ['field' => 'dikdas', 'label' => 'Pendidikan Dasar', 'rules' => 'required']
    ]);

    if ($validation->run()) {
      if ($this->input->post()) {
        $proses = $this->modref->savedatapeg();
        if ($proses) {
          $this->session->set_flashdata('success', 'Proses berhasil');
        } else {
          $this->session->set_flashdata('error', 'Proses GAGAL');
        }
      }
    } else {
      $this->session->set_flashdata('error', 'Data Not Valid');
    }
    redirect('setting/pegawai');
  }
  public function syncdatapeg()
  {
    if ($this->input->post('syncpeg')) {
      $this->load->model('modref');
      $proses = $this->modref->syncdatapeg();
      if ($proses) {
        $this->session->set_flashdata('success', 'Proses berhasil, ' . $proses . ' data ditambahkan');
      } else {
        $this->session->set_flashdata('warning', 'Proses GAGAL / Tidak ada data baru');
      }
    }
    redirect('setting/pegawai');
  }

  public function refjenjang()
  {
    $data['banner'] = false;
    $data['page'] = 'refjenjang';
    $data['judul'] = 'Setting Referensi Jenjang Pendidikan';
    $data['content']['action'] = '';
    $data['content']['dtrefjenjang'] = $this->modref->getrefjenjang('1=1');
    $this->load->view('mainview', $data);
  }

  public function formjenjang($id = null)
  {
    if ($id) {
      $data['edit'] = true;
      $data['dtjenjang'] = $this->modref->getrefjenjang("id_jenjang='$id'");
    } else {
      $id = 1;
      if ($lastrec = $this->modref->getrefjenjang('true order by id_jenjang desc limit 1')) $id = $lastrec[0]->id_jenjang + 1;
      $data['idjenjang'] = $id;
      $data['edit'] = false;
    }
    $data['judul'] = 'Form Ref. Jenjang Pendidikan';
    $data['action'] = 'setting/refjenjangsave';
    $this->load->view('formjenjang', $data);
  }

  public function refjenjangsave()
  {
    $validation = $this->form_validation;
    $validation->set_rules([
      ['field' => 'idjenjang', 'label' => 'ID Jenjang Pendidikan', 'rules' => 'required'],
      ['field' => 'nmjenjang', 'label' => 'jenjang Pendidikan', 'rules' => 'required'],
      ['field' => 'ket', 'label' => 'Keterangan', 'rules' => 'required'],
      ['field' => 'stat', 'label' => 'Status', 'rules' => 'required']
    ]);

    if ($validation->run()) {
      if ($this->input->post()) {
        $proses = $this->modref->saverefjenjang();
        if ($proses) {
          $this->session->set_flashdata('success', 'Proses berhasil');
        } else {
          $this->session->set_flashdata('error', 'Proses GAGAL');
        }
      }
    } else {
      $this->session->set_flashdata('error', 'Data Not Valid');
    }
    redirect('setting/refjenjang');
  }

  public function refuniv()
  {
    $data['banner'] = false;
    $data['page'] = 'refuniv';
    $data['judul'] = 'Setting Referensi Perguruan Tinggi';
    $data['content']['action'] = '';
    $data['content']['dtrefuniv'] = $this->modref->getrefuniv('1=1');
    $this->load->view('mainview', $data);
  }

  public function formuniv($id = null)
  {
    if ($id) {
      $data['edit'] = true;
      $data['dtuniv'] = $this->modref->getrefuniv("id_univ='$id'");
    } else {
      $id = 1;
      if ($lastrec = $this->modref->getrefuniv('true order by id_univ desc limit 1')) $id = $lastrec[0]->id_univ + 1;
      $data['iduniv'] = $id;
      $data['edit'] = false;
    }
    $data['judul'] = 'Form Ref. Perguruan Tinggi';
    $data['action'] = 'setting/formunivsave';
    $data['uri'] = $this->input->post('uri');
    $this->load->view('formuniv', $data);
  }

  public function formunivsave()
  {
    $validation = $this->form_validation;
    $validation->set_rules([
      ['field' => 'iduniv', 'label' => 'ID Universitas', 'rules' => 'required'],
      ['field' => 'nmuniv', 'label' => 'Universitas', 'rules' => 'required'],
      ['field' => 'lokasi', 'label' => 'Lokasi', 'rules' => 'required'],
      ['field' => 'statuniv', 'label' => 'Status', 'rules' => 'required']
    ]);

    if ($validation->run()) {
      if ($this->input->post()) {
        $proses = $this->modref->saverefuniv();
        if ($proses) {
          $this->session->set_flashdata('success', 'Proses berhasil');
        } else {
          $this->session->set_flashdata('error', 'Proses GAGAL');
        }
      }
    } else {
      $this->session->set_flashdata('error', 'Data Not Valid');
    }
    if ($uri = $this->input->post('uri')) {
      redirect($uri);
    }
  }

  public function resetpass($iduser)
  {
    if ($iduser && $this->session->userdata('iduser') === '1') {
      if ($this->db->update('tuser', ['password' => sha1('123456')], ['iduser' => $iduser])) {
        $this->session->set_flashdata('success', 'Reset Sandi berhasil.');
      } else {
        $this->session->set_flashdata('error', 'Reset Sandi GAGAL.');
      }
    }
    redirect('setting/user');
  }
}
