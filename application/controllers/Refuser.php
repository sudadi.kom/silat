<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';
class Refuser extends MY_Controller {    
    
    private $aksesmenu = [];
    
    function __construct() {
       parent::__construct();
        //kode menu ref user -> 13
       if ($this->session->userdata('usrmsk')==NULL) {
           redirect('main');
       } else {
           $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
            if ($this->session->userdata('idunit') !=='1' && !in_array('6', $this->aksesmenu)){
                redirect('main');   
            }
        }
    }
    
    function index(){
        $this->load->model('mref');
        $edit = $idunit = $iduser = $nama = $password = $status = '';
        if ($this->input->post()){
            $edit = $this->input->post('edit');
            $idunit = $this->input->post('idunit');
            $nama = $this->input->post('nama');
            $password = sha1($this->input->post('password'));
            $iduser = $this->input->post('iduser');
            $status = $this->input->post('status');
            if ($edit){
                $this->db->update('user', array('userid'=>$iduser, 'password'=>$password, 'nama'=>$nama, 'idunit'=>$idunit, 'status'=>$status), array('id'=>$edit));
                if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Update data BERHASIL');
                } else {
                    $this->session->set_flashdata('error', 'Update data GAGAL!');
                }
            } else {
                $this->db->insert('user', array('userid'=>$iduser, 'password'=>$password, 'nama'=>$nama, 'idunit'=>$idunit, 'status'=>$status));
                if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Tambah data BERHASIL');
                } else {
                    $this->session->set_flashdata('error', 'Tambah data GAGAL!');
                }
            }
            redirect(base_url('refuser'));
        } else if ($this->input->get()) {
            $edit = $this->input->get('edit');
            $hapus = $this->input->get('hapus');
            if ($edit){
                $result = $this->db->get_where('user', array('id'=>$edit))->row();
                if ($result){
                    $iduser = $result->userid;
                    $nama = $result->nama;
                    $idunit = $result->idunit;
                    $status = $result->status;
                } 
            } else if($hapus){
                $this->db->delete('user', array('id'=>$hapus));
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('success', 'Penghapusan data BERHASIL.');
                } else {
                    $this->session->set_flashdata('success', 'Penghapusan data GAGAL!');
                }
                redirect(base_url('refuser'));
            }            
        } 
        
        $content = array('akses'=> $this->aksesmenu, 'idunit'=>$idunit, 'nama'=>$nama, 'edit'=>$edit, 'iduser'=>$iduser, 'status'=>$status);
        $data['content']= $content;
        $data['content']['result'] = $this->modpelatihan->getusulan($thn, 'all', 3);
        $data['page'] = 'refuserpage';
        $this->load->view('main', $data);
    }
}