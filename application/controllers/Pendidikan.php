<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Pendidikan extends CI_Controller{
  function __construct() {
    parent:: __construct();
    if ($this->session->userdata('usrmsk')==NULL) {
      redirect('main');
    } else if ($this->session->userdata('iduser') !=1) {
      $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
      if (!in_array($menu, $aksesmenu)){
        redirect('main');
      }
    } 
  }
  
  public function index($idpeg=null) {
    $dtpegawai = $dtpendidikan = '';
    if ((($idpeg) || ($idpeg = $this->input->post('idpeg'))) && ($dtpegawai = $this->modref->getdatapeg(['id_pegawai'=>$idpeg]))) {
      $this->load->model('Modpelatihan');
      $dtpendidikan = $this->Modpelatihan->getdatadidik(['pegawai_id'=>$idpeg]);
    } 
    $data['banner'] = false;
    $data['page'] = 'didikpage';
    $data['judul'] = 'Data Pendidikan Pegawai';
    $data['content']['action'] = 'Pendidikan/';
    $data['content']['pegawai'] = $this->modref->getpegawai();
    $data['content']['dtpegawai'] = $dtpegawai;
    $data['content']['idpeg'] = $idpeg;
    $data['content']['dtpendidikan'] = $dtpendidikan;
    $this->load->view('mainview', $data);
  }
  
  public function formpendidikan($idpeg, $iddidik=null) {
    $this->load->model('Modpelatihan');
    if ($data['idpeg']=$idpeg) {
      if ($iddidik) {
        $data['edit']=true;
        $data['dtdidik'] = $this->Modpelatihan->getpendidikan("id='$iddidik'");
      } else {
        $id = 1;
        if ($lastrec = $this->Modpelatihan->getpendidikan('true order by id desc limit 1')){
          $id = $lastrec[0]->id + 1;
        }
        $data['id'] = $id;
        $data['edit']=false;
      }
      $data['refjenjang'] = $this->modref->getrefjenjang('1=1');
      $data['refuniv'] = $this->modref->getrefuniv('1=1');
      $data['judul']='Form Pendidikan';
      $data['action']= 'pendidikan/formdidiksave';
      $this->load->view('formpendidikan', $data);
    }
  }
  
  public function formdidiksave() {
    $validation = $this->form_validation;
    $validation->set_rules([
      ['field' => 'id','label' => 'ID','rules' => 'required'],
      ['field' => 'univid','label' => 'Universitas','rules' => 'required'],
      ['field' => 'jenjangid','label' => 'Jenjang','rules' => 'required'],
      ['field' => 'tipe','label' => 'Jenis Pendidikan','rules' => 'required'],
      ['field' => 'fak','label' => 'Fakultas','rules' => 'required'],
      ['field' => 'idpeg','label' => 'Pegawai','rules' => 'required']
    ]);

    if ($validation->run()) {        
      if ($this->input->post()) {
        $this->load->model('Modpelatihan');
        $proses = $this->Modpelatihan->savependidikan();
        //echo $this->db->last_query(); die();
        if ($proses) {
          $this->session->set_flashdata('success', 'Proses berhasil');
        } else {
          $this->session->set_flashdata('error', 'Proses GAGAL');
        }
      }
    } else {
      $this->session->set_flashdata('error', 'Data Not Valid');
    }
    redirect('pendidikan/index/'.$this->input->post('idpeg'));
  }
  
  public function hapusdidik($idpeg, $iddidik) {
    if (($idpeg && $iddidik)) {
      if ($this->db->delete("tpendidikan", "id='$iddidik'")) {
        $this->session->set_flashdata('success', 'Proses Berhasil');
      } else {
        $this->session->set_flashdata('error', 'Proses GAGAL');
      }
      redirect('pendidikan/index/'.$idpeg);
    }
  }
}