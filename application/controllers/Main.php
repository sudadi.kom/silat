<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
            
    }

    public function index()
    {      
      $this->load->model('modlaporan');
      $tahun = date("Y");
      $data['banner'] = TRUE;
      $data['page'] = 'infopage';
      $data['judul'] = 'Informasi';
      $data['content']['realisasi']= json_encode($this->modlaporan->dashRealplt('idjenis="2" and DATE_FORMAT(mulai, "%Y")="'.$tahun.'" group by tgl'));
        $this->load->view('mainview', $data);
                
    }
    
    public function login() 
    {
      $this->form_validation->set_rules('username', 'username', 'required|trim');
      $this->form_validation->set_rules('password', 'password', 'required|trim');
      if ($this->form_validation->run() == TRUE) {
          $username=$this->input->post('username');
          $password=$this->input->post('password');
          $this->load->model('moduser');
          $result=$this->moduser->login($username, $password);
          if ($result) {
              $this->session->set_userdata('usrmsk', 'TRUE');
              $this->session->set_userdata('iduser', $result->iduser);
              $this->session->set_userdata('username', $result->username);
              $this->session->set_userdata('realname', $result->nama);
              $this->session->set_userdata('passupd', $result->updpass);
              $unit = $this->modref->getunit($result->idunit);
              if ($unit) {
                  foreach ($unit as $row){
                  $this->session->set_userdata('namaunit', $row->nama_unit_kerja);
                  $this->session->set_userdata('idunit', $row->id_unit_kerja);
                  $this->session->set_flashdata('success', 'Selamat datang '.$result->nama);
                  }
              } else {
                  $this->session->set_flashdata('warning', 'Satker Tidak Ditemukan!!');
              }
              $this->db->update('tuser', array('lastlog'=>date('Y/m/d h:i:s')), array('username'=>$result->username));
              redirect('main');	
          } else {			
              $this->session->set_flashdata('error', 'Your username or password are incorrect');
          }
      }   

      $data['banner'] = false;
      $data['page'] = 'loginpage';
      $data['judul'] = 'Login';
      $data['content']['action'] = base_url('main/login');
      $this->load->view('mainview', $data);
    }
    
    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
