<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Rencana extends CI_Controller{
   
    function __construct() {
        parent:: __construct();
        
        $menu = 4;
        if ($this->session->userdata('usrmsk')==NULL) {
            redirect('main');
        } else if ($this->session->userdata('iduser') !=1) {
            $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
            if (!in_array($menu, $aksesmenu)){
                redirect('main');
            }
        }             
    }
    
    public function index() {
        $this->load->model('modpelatihan');
        $thn =  date('Y') +1;
        $data['banner'] = false;
        $data['page'] = 'rencanapage';
        $data['judul'] = 'Perencanaan Pelatihan';
        $data['content']['result'] = $this->modpelatihan->getusulan(NULL, 'all', 3);
        $this->load->view('mainview', $data);
    }
    
    public function formrencana() {
        $data['action'] = site_url('rencana/proses');
        $data['title'] = 'Perencanaan Pelatihan';
        $data['idplt'] = $this->input->post('idplt');
        $data['jpl'] = 0;
        $data['tempat'] = '';
        $data['mulai']  = date("Y/m/d");
        $data['selesai'] = date("Y/m/d");
        $this->load->view('formjadwal', $data);
    }
    
    public function proses() {
        if ($this->input->post()) {
            $idplt = $this->input->post('idplt');
            $mulai = $this->input->post('mulai');
            $selesai = $this->input->post('selesai');
            $tempat = $this->input->post('tempat');
            $jpl = $this->input->post('jpl');
            $this->db->insert('tjadwal', array('idplt'=>$idplt, 'idjenis'=>1, 'mulai'=>$mulai, 'selesai'=>$selesai, 'tempat'=>$tempat, 'jpl'=>$jpl));
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success', 'Data Berhasil disimpan');
            }else{
                $this->session->set_flashdata('error', 'Proses GAGAL');
            }
        }
        redirect('rencana');
    }
    
}