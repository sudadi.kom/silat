<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Laporan extends CI_Controller{
   
  private $thn, $bln; 

  function __construct() {
      parent:: __construct();
      $this->load->library('m_pdf');
      $this->load->model('modlaporan');
      $menu = 5;
      if ($this->session->userdata('usrmsk')==NULL) {
          redirect('main');
      } else if ($this->session->userdata('iduser') !=1) {
          $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
          if (!in_array($menu, $aksesmenu)){
              redirect('main');
          }
      }  
      if ($this->input->post('tahun')) {
          $this->thn = $this->input->post('tahun');
      }else{
          $this->thn = date("Y");
      }
      if (!($this->bln = $this->input->post('bulan'))) {
        $this->bln = date ("m"); 
      }
  }

  public function index($param) {
      redirect('main');
  }

  public function laprekap() {
      $data['content']['tahun'] = $this->thn;
      $data['banner'] = false;
      $data['page'] = 'laprekappage';
      $data['judul'] = 'Laporan Rekap Usulan Pelatihan';
      $data['content']['action'] = site_url('laporan/laprekap');
      $data['content']['result'] = $this->modlaporan->getlaprekap($this->thn, 2);

      $this->load->view('mainview', $data);
  }

  function cetaklaprekap($vthn) {
      $data['judul'] = 'Laporan Rekap Usulan Pelatihan';
      $data['result'] = $this->modlaporan->getlaprekap($vthn, 2);
      $data['thn'] = $vthn;
      $html = $this->load->view('laprekappdf', $data, TRUE);
      $pdfFilePath = 'laprekap-'.$vthn.'.pdf';
      $this->cetakpdf($html, $pdfFilePath, 'P');
  }

  public function lapjadwal() {
      $data['content']['tahun'] = $this->thn;
      $data['banner'] = false;
      $data['page'] = 'lapjadwalpage';
      $data['judul'] = 'Rekap Jadwal Pelaksanaan Pelatihan';
      $data['content']['action'] = site_url('laporan/lapjadwal');
      $data['content']['result'] = $this->modlaporan->getlaprekap($this->thn, 4);

      $this->load->view('mainview', $data);
  }

  function cetaklapjadwal($vthn) {
      $data['judul'] = 'Rekap Jadwal Pelaksanaan Pelatihan';
      $data['result'] = $this->modlaporan->getlaprekap($vthn, 4);
      $data['thn'] = $vthn;
      $html =  $this->load->view('lapjadwalpdf', $data, TRUE);
      $pdfFilePath = 'lapjadwal-'.$vthn.'.pdf';
      $this->cetakpdf($html, $pdfFilePath, 'P');
  }

  public function lapresume() {
    if (($start = $this->input->post('start')) && ($end = $this->input->post('end'))) {          
      $data['content']['result'] = $this->modlaporan->getresume($start, $end);
      $data['content']['start'] = $start;
      $data['content']['end'] = $end;
    } else {
      $data['content']['result'] = NULL;
      $data['content']['start'] = date('Y-m-d');
      $data['content']['end'] = date('Y-m-d');
    }
      $data['banner'] = false;
      $data['page'] = 'laprespage';
      $data['judul'] = 'Resume Pelaksanaan Pelatihan';
      $data['content']['action'] = site_url('laporan/lapresume');        
      $this->load->view('mainview', $data);
  }

  function cetaklapresume($start, $end) {
      $data['judul'] = 'Resume Pelaksanaan Pelatihan';
      $data['result'] = $this->modlaporan->getresume($start, $end);
      $data['start'] = $start;
      $data['end'] = $end;
      //$this->load->view('laprespdf', $data, TRUE);
      $html =  $this->load->view('laprespdf', $data, TRUE);
      $pdfFilePath = './lapresume.pdf';
      $this->cetakpdf($html, $pdfFilePath, 'L');
  }    

  public function export_excel($start, $end){
      $data['judul'] = 'Resume Pelaksanaan Pelatihan';
      $data['result'] = $this->modlaporan->getresume($start, $end);
      $data['start'] = $start;
      $data['end'] = $end;
      $this->load->view('lapresxls', $data);
  }
  
  public function resdidik() {    
    $where = NULL;
    if ($this->input->post('thnMasuk')) {
      $where['thn_masuk'] = $this->input->post('thnMasuk');
    }
    if ($this->input->post('thnLulus')) {
      $where['thn_lulus'] = $this->input->post('thnLulus');
    }
    if (($this->input->post('status') == 0) || ($this->input->post('status') == 1)) {
      $where['tpendidikan.status'] = $this->input->post('status');
    }
    $this->load->model('Modpelatihan');
    $data['content']['tahun'] = $this->thn;
    $data['content']['dtpendidikan'] = $this->Modpelatihan->getdatadidik($where);
    echo $this->input->post('status');
    //echo $this->db->last_query(); die();
    $data['banner'] = false;
    $data['page'] = 'resdidikpage';
    $data['judul'] = 'Resume Data Pendidikan';
    $data['content']['action'] = site_url('laporan/resdidik');

    $this->load->view('mainview', $data);
  }
  
  public function resliti() {
    $this->load->model('Modpelatihan');
    $data['content']['tahun'] = $this->thn;
    $data['content']['dtpenelitian'] = $this->Modpelatihan->getdataliti(['thn'=>$this->thn]);
    $data['banner'] = false;
    $data['page'] = 'reslitipage';
    $data['judul'] = 'Resume Data Penelitian';
    $data['content']['action'] = site_url('laporan/resliti');

    $this->load->view('mainview', $data);
  }

  public function silatres() {
    $dtpegawai = $dtpenelitian = $dtpendidikan = $dtpelatihan ='';
    if (($idpeg = $this->input->post('idpeg')) && ($dtpegawai = $this->modref->getdatapeg(['id_pegawai'=>$idpeg]))) {
      $this->load->model('Modpelatihan');
      $dtpenelitian = $this->Modpelatihan->getdataliti(['pegawai_id'=>$idpeg]);
      $dtpendidikan = $this->Modpelatihan->getdatadidik(['pegawai_id'=>$idpeg]);
      $dtpelatihan = $this->Modpelatihan->getdatalatih($idpeg);
    }
    $data['banner'] = false;
    $data['page'] = 'silatrespage';
    $data['judul'] = 'Resume Pendidikan-Pelatihan-Penelitian';
    $data['content']['action'] = site_url('laporan/silatres'); 
    $data['content']['pegawai'] = $this->modref->getpegawai();
    $data['content']['idpeg'] = $idpeg;
    $data['content']['dtpegawai'] = $dtpegawai;
    $data['content']['dtpenelitian'] = $dtpenelitian;
    $data['content']['dtpendidikan'] = $dtpendidikan;
    $data['content']['dtpelatihan'] = $dtpelatihan;
    $this->load->view('mainview', $data);
  }    

  function cetakpdf($html, $pdfFilePath, $page) {
      $pdf = $this->m_pdf->load();

      $pdf->AddPage($page);
      $pdf->WriteHTML($stylesheet, 1);
      $pdf->WriteHTML($html);

      $pdf->Output($pdfFilePath, "I");
      exit(); 
  }
}