<?php defined('BASEPATH') or exit('No direct access script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Peserta extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('modpelatihan');
        if ($this->session->userdata('usrmsk')==NULL) {
            redirect('main');
        }
    }
    
    public function index($param) {
        redirect('main');
    }
    
    public function daftar($idplt, $susulan=null) {
        $thn =  date('Y') +1;
        
        switch ($susulan) {
          case 1 :
            $url = base_url('persetujuan');
            break;
          case 2 :
            $url = base_url('rencana');
            break;
          case 1 :
            $url = base_url('realisasi');
            break;
          default:
            $url = base_url('usulan');
        break;
        }
          
        $data['banner'] = false;
        $data['page'] = 'pesertapage';
        $data['judul'] = 'Daftar Peserta Pelatihan ['.$this->modpelatihan->getnmplt($idplt)->nmplt.']';
        $data['content']['action'] = base_url('peserta/save');
        $data['content']['idplt']= $idplt;
        $data['content']['backurl']= $url;
        $data['content']['susulan']= $susulan;
        $data['content']['result'] = $this->modpelatihan->getpeserta($idplt);
        
        $this->load->view('mainview', $data);
    }
    
    public function save() {
        if ($this->input->post()) {
            $idplt = $this->input->post('idplt');
            $idpeg = $this->input->post('peserta');
            $susulan = $this->input->post('susulan');
            if ($this->modpelatihan->savepeserta($idplt, $idpeg, $susulan, 1)) {
                 $this->session->set_flashdata('success', 'Data Tersimpan');
            }else{
                 $this->session->set_flashdata('error', 'Proses simpan gagal');
            }
        }
        redirect(base_url().'peserta/daftar/'.$idplt.'/'.$susulan);
    }
    
    public function hapus($idpeserta, $idplt) {
        if ($idpeserta != NULL && $idplt != NULL) {
            $this->db->delete('tpeserta', array('idpeserta'=>$idpeserta, 'idplt'=>$idplt));
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success', 'Data sudah dihapus');
            } else {
                $this->session->set_flashdata('error', 'Hapus data gagal');
            }
        }
        redirect(base_url().'peserta/daftar/'.$idplt);
    }
    
     public function updstatus($idpeserta, $idplt, $sesuai, $susulan) {
        if ($idpeserta != NULL && $idplt != NULL) {
            $this->db->where(array('idpeserta'=>$idpeserta, 'idplt'=>$idplt));
            $this->db->update('tpeserta', array('sesuai'=>!$sesuai));
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success', 'Data sudah diupdate');
            } else {
                $this->session->set_flashdata('error', 'Update data gagal');
            }
        }
        redirect(base_url().'peserta/daftar/'.$idplt.'/'.$susulan);
    }

}


?>
