<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Realisasi extends CI_Controller{
   
    function __construct() {
        parent:: __construct();
        $this->load->model('modpelatihan');
        
        $menu = 5;
        if ($this->session->userdata('usrmsk')==NULL) {
            redirect('main');
        } else if ($this->session->userdata('iduser') !=1) {
            $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
            if (!in_array($menu, $aksesmenu)){
                redirect('main');
            }
        }             
    }
    
    public function index() {
        $thn =  date('Y') +1;
        $data['banner'] = false;
        $data['page'] = 'realisasipage';
        $data['judul'] = 'Realisasi Pelatihan';
        $data['content']['result'] = $this->modpelatihan->getusulan(NULL, 'all', 4);
        $this->load->view('mainview', $data);
    }
    
    public function formrealisasi() {
        $data['action'] = site_url('realisasi/proses');
        $data['title'] = 'Realisasi Pelatihan';
        $data['idplt'] = $this->input->post('idplt');
        $data['idjenis'] = 2;
        $jadwal = $this->modpelatihan->getjadwal($data['idplt'], 2);
        if (!$jadwal) {
            $jadwal = $this->modpelatihan->getjadwal($data['idplt'], 1);
        }
        $data['jpl'] = $jadwal->jpl;
        $data['tempat'] = $jadwal->tempat;
        $data['mulai']  = $jadwal->mulai;
        $data['selesai'] = $jadwal->selesai;
        $this->load->view('formjadwal', $data);
    }
    
    public function proses() {
        if ($this->input->post()) {
            $idplt = $this->input->post('idplt');
            $mulai = $this->input->post('mulai');
            $selesai = $this->input->post('selesai');
            $tempat = $this->input->post('tempat');
            $jpl = $this->input->post('jpl');
            if ($this->db->get_where('tjadwal', array('idplt' => $idplt, 'idjenis'=>2))->num_rows()> 0 ) {
                $this->db->where(array('idplt' => $idplt, 'idjenis'=>2));
                $this->db->update('tjadwal', array('mulai'=>$mulai, 'selesai'=>$selesai, 'tempat'=>$tempat, 'jpl'=>$jpl));
            } else {
                $this->db->insert('tjadwal', array('idplt'=>$idplt, 'idjenis'=>2, 'mulai'=>$mulai, 'selesai'=>$selesai, 'tempat'=>$tempat, 'jpl'=>$jpl));
            }
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success', 'Data Berhasil disimpan');
            }else{
                $this->session->set_flashdata('error', 'Proses GAGAL');
            }
        }
        redirect('realisasi');
    }
    
    public function peserta($idplt) {
        if ($this->session->userdata('iduser') == 1){
            $susulan = 1;
        }else {
            $susulan = 0;
        }
        $thn =  date('Y') +1;
        $data['banner'] = false;
        $data['page'] = 'pesertapage';
        $data['judul'] = 'Daftar Peserta Pelatihan ['.$this->modpelatihan->getnmplt($idplt)->nmplt.']';
        $data['content']['action'] = base_url('peserta/save');
        $data['content']['idplt']= $idplt;
        $data['content']['susulan']=$susulan;
        $data['content']['result'] = $this->modpelatihan->getpeserta($idplt);
        
        $this->load->view('mainview', $data);
    }
    
    public function updstat($idplt) {
        if ($idplt != NULL) {
            $this->db->insert('thistori', array('idplt'=>$idplt, 'idstatus'=>5));
            if ($this->db->affected_rows()>0) {
                $this->session->set_flashdata('success', 'Update data selesai');
            } else {
                $this->session->set_flashdata('error', 'Update status GAGAL');
            }
        }
        redirect('realisasi');
    }
}