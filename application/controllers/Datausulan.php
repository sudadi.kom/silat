<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Datausulan extends CI_Controller{
   
    function __construct() {
        parent:: __construct();
        
        $this->load->model('modpelatihan');
        $menu = 2;
        if ($this->session->userdata('usrmsk')==NULL) {
            redirect('main');
        } else if ($this->session->userdata('iduser') !=1) {
            $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
            if (!in_array($menu, $aksesmenu)){
                redirect('main');
            }
        }             
    }
    
    public function index() {
        $thn =  date('Y') +1;
        $data['banner'] = false;
        $data['page'] = 'datausulpage';
        $data['judul'] = 'Data Semua Usulan Pelatihan';
        $data['content']['result'] = $this->modpelatihan->getusulan(NULL, 'all', 1);
        
        $this->load->view('mainview', $data);
    }    
    
    public function proses($idplt) {
        if ($idplt != null) {
            $result = $this->modpelatihan->updstat($idplt, 2);
            if ($result > 0) {
                $this->session->set_flashdata('success', 'Proses Berhasil');
            } else {
                $this->session->set_flashdata('error', 'Proses Gagal');
            }
        }
        redirect('datausulan');
    }
}