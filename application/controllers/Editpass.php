<?php defined('BASEPATH') or exit('No direct access script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Editpass extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('moduser');
        if ($this->session->userdata('usrmsk')==NULL) {
            redirect('main');
        }
    }
    
    public function index() {
        $data['banner'] = FALSE;
        $data['page'] = 'editpasspage';
        $data['judul'] = 'Form Ubah Password';
        $data['content']['action']= '/editpass/update';
        $this->load->view('mainview', $data);
    }
    
    public function update() {
        if ($this->input->post()){
            $username = $this->session->userdata('username');
            $oldpass = $this->input->post('oldpass');
            $newpass1 = $this->input->post('newpass1');
            $newpass2 = $this->input->post('newpass2');
            if ($newpass1 == $newpass2) {
                $result = $this->moduser->login($username, $oldpass);
                if ($result){
                    $this->db->update('tuser', array('password'=> sha1($newpass1), 'updpass'=>date('Y/m/d')), array('username'=>$username));
                    if ($this->db->affected_rows() > 0) {
                        $this->session->set_flashdata('success', 'Perubahan Password BERHASIL');
                        $this->session->set_userdata('passupd', date('Y/m/d'));
                    } else {
                        $this->session->set_flashdata('error', 'Perubahan Password GAGAL!');
                    }
                } else {
                    $this->session->set_flashdata('error', 'GAGAL, Password lama yang anda masukkan salah!');
                }                  
            } else {
                $this->session->set_flashdata('error', 'GAGAL, Password baru tidak sama!');
            }
        }
        redirect(base_url('/editpass'));
    }
    
}
