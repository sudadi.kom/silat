<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Penelitian extends CI_Controller{
  function __construct() {
    parent:: __construct();
    if ($this->session->userdata('usrmsk')==NULL) {
      redirect('main');
    } else if ($this->session->userdata('iduser') !=1) {
      $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
      if (!in_array($menu, $aksesmenu)){
        redirect('main');
      }
    } 
  }
  
  public function index($idpeg=null) {
    $dtpegawai = $dtpenelitian = '';
    if ((($idpeg) || ($idpeg = $this->input->post('idpeg'))) && ($dtpegawai = $this->modref->getdatapeg(['id_pegawai'=>$idpeg]))) {
      $this->load->model('Modpelatihan');
      $dtpenelitian = $this->Modpelatihan->getdataliti(['pegawai_id'=>$idpeg]);
    } 
    $data['banner'] = false;
    $data['page'] = 'litipage';
    $data['judul'] = 'Data Penelitian';
    $data['content']['action'] = 'Penelitian/';
    $data['content']['pegawai'] = $this->modref->getpegawai();
    $data['content']['dtpegawai'] = $dtpegawai;
    $data['content']['idpeg'] = $idpeg;
    $data['content']['dtpenelitian'] = $dtpenelitian;
    $this->load->view('mainview', $data);
  }
  
  public function formpenelitian($idpeg, $idliti=null) {
    $this->load->model('Modpelatihan');
    if ($data['idpeg']=$idpeg) {
      if ($idliti) {
        $data['edit']=true;
        $data['dtliti'] = $this->Modpelatihan->getpenelitian("id=1");
      } else {
        $id = 1;
        if ($lastrec = $this->Modpelatihan->getpenelitian('true order by id desc limit 1')){
          $id = $lastrec[0]->id + 1;
        }
        $data['id'] = $id;
        $data['edit']=false;
      }
      $data['judul']='Form Penelitian';
      $data['action']= 'penelitian/formlitisave';
      $this->load->view('formliti', $data);
    }
  }
  
  public function formlitisave() {
    $validation = $this->form_validation;
    $validation->set_rules([
      ['field' => 'id','label' => 'ID','rules' => 'required'],
      ['field' => 'judul','label' => 'Judul Penelitian','rules' => 'required'],
      ['field' => 'link','label' => 'Publikasi Link','rules' => 'required'],
      ['field' => 'thn','label' => 'Tahun','rules' => 'required'],
      ['field' => 'idpeg','label' => 'Pegawai','rules' => 'required']
    ]);

    if ($validation->run()) {
      $this->load->model('Modpelatihan');
      $proses = $this->Modpelatihan->savepenelitian();
      if ($proses > 0) {
        $this->session->set_flashdata('success', 'Proses berhasil');
      } else {
        $this->session->set_flashdata('error', 'Proses GAGAL');
      }
    } else {
      $this->session->set_flashdata('error', 'Data Not Valid');
    }
    redirect('penelitian/index/'.$this->input->post('idpeg'));
  }
  
  public function hapusliti($idpeg, $idliti) {
    $this->load->model('Modpelatihan');
    if (($idpeg && $idliti)) {
      $file = $this->Modpelatihan->getpenelitian(['id'=>$idliti])[0]->data;
      if ($this->db->delete("tpenelitian", "id='$idliti'")) {
        $this->deleteFile($file); 
        $this->session->set_flashdata('success', 'Proses Berhasil');
      } else {
        $this->session->set_flashdata('error', 'Proses GAGAL');
      }
      redirect('penelitian/index/'.$idpeg);
    } else {
      $this->session->set_flashdata('error', 'Proses GAGAL');
    }
  }
  
  private function deleteFile($file) {
    return array_map('unlink', glob(FCPATH."upload/penelitian/$file"));
  }
  
  public function download($file) {
    if ($file) {
      $this->load->helper('download');
      force_download('upload/penelitian/'.$file, NULL);
    }
  }
}