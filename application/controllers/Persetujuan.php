<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Persetujuan extends CI_Controller{
   
    function __construct() {
        parent:: __construct();
        
        $this->load->model('modpelatihan');
        $menu = 3;
        if ($this->session->userdata('usrmsk')==NULL) {
            redirect('main');
        } else if ($this->session->userdata('iduser') !=1) {
            $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
            if (!in_array($menu, $aksesmenu)){
                redirect('main');
            }
        }             
    }
    
    public function index() {
        $thn =  date('Y') +1;
        $data['banner'] = false;
        $data['page'] = 'persetujuanpage';
        $data['judul'] = 'Persetujuan Usulan Pelatihan';
        $data['content']['result'] = $this->modpelatihan->getusulan(NULL, 'all', 2);
        $this->load->view('mainview', $data);
    }
    
    public function formtolak() {
        $data['action'] = site_url('persetujuan/tolak');
        $data['idplt'] = $this->input->post('idplt');
        $this->load->view('formtolak', $data);
    }
    
    public function proses($idplt, $stat, $ket=null) {
        if ($idplt != NULL) {
            $result = $this->modpelatihan->updstat($idplt, $stat, $ket);
            if ($result > 0) {
                $this->session->set_flashdata('success', 'Proses Berhasil');
            } else {
                $this->session->set_flashdata('error', 'Proses Gagal');
            }
        }
        redirect('persetujuan');
    }
    
    public function tolak() {
        if ($this->input->post()) {
            $idplt = $this->input->post('idplt');
            $ket = $this->input->post('ket');
            $stat = 6;
            $this->proses($idplt, $stat, $ket);
        }
    }
}