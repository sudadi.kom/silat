<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Usulan extends CI_Controller{
   
    function __construct() {
        parent:: __construct();
        
        $this->load->model('modpelatihan');
        $menu = 1;
        if ($this->session->userdata('usrmsk')==NULL) {
            redirect('main');
        } else if ($this->session->userdata('iduser') !=1) {
            $aksesmenu = $this->moduser->getakses($this->session->userdata('iduser'));
            if (!in_array($menu, $aksesmenu)){
                redirect('main');
            }
        }  
                
    }
    
    public function index() {
        $thn =  date('Y') +1;
        $data['banner'] = false;
        $data['page'] = 'usulanpage';
        $data['judul'] = 'Pengajuan Usulan Pelatihan';
        $data['content']['result'] = $this->modpelatihan->getusulan(NULL, $this->session->userdata('idunit'), 0);
        $data['content']['onoff'] = $this->modpelatihan->getonoff();
        $this->load->view('mainview', $data);
    }
    
    public function formusulan($idplt=null) {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $plt = null;
        if ($idplt) {
            $this->load->model('modpelatihan');
            $plt= $this->modpelatihan->getnmplt($idplt);
        }
        $data['action'] = site_url('usulan/save');
        $data['plt'] = $plt;
        $this->load->view('formusulan', $data);
    }

    public function save() {
        if ($this->input->post()) {
            $idunit = $this->session->userdata('idunit');
            $tahun = $this->input->post('tahun');
            $pelatihan = $this->input->post('pelatihan');
            $uraian = $this->input->post('uraian');
            if(!empty($this->input->post('idplt'))) {
                $this->db->delete('thistori', ['idplt'=>$this->input->post('idplt'), 'idstatus'=>1]);
                $this->db->update('tusulan', array('tahun'=>$tahun,'nmplt'=>$pelatihan,'uraian'=>$uraian), ['idplt'=>$this->input->post('idplt')]);
            } else {            
                $this->db->insert('tusulan', array('tahun'=>$tahun, 'id_unit_kerja'=>$idunit, 'nmplt'=>$pelatihan, 
                'uraian'=>$uraian));
            }
            if ($this->db->affected_rows()>0){
                $this->session->set_flashdata('success', 'Data sudah tersimpan');
            } else {
                $this->session->set_flashdata('error', 'Data tidak dapat di simpan');
            }                
            redirect('usulan');
        }
    }
    
    public function hapus($idplt) {
        if ($idplt !== NULL) {
            $this->db->delete('tusulan', array('idplt'=>$idplt));
            if ($this->db->affected_rows()>0) {
                $this->session->set_flashdata('success', 'Data sudah dihapus');
            } 
        } else {
            $this->session->set_flashdata('success', 'null');
        }
        redirect('usulan');
    }
    
    public function kirim($idplt) {
        if ($idplt != NULL) {
            if ($this->modpelatihan->updstat($idplt, 1) > 0) { //1. status Diusulkan
                $this->session->set_flashdata('success', 'Data sudah tersimpan');
            }
        }
        redirect('usulan');
    }
    
  

}