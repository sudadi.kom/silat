<?php defined('BASEPATH') or exit('No dirrect script aceess allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-sm-6 text-right">
    <button type="button" class="btn btn-primary" id="tambah" onclick="Editpeg(false);"><i class="fa fa-plus"></i> Tambah Pegawai</button>
  </div>
  <!-- <div class="col-sm-6 text-left">
    <?= form_open($action); ?>
      <button type="submit" class="btn btn-success" id="sync" name="syncpeg" value="true"><i class="fa fa-refresh"></i> Sync DB Simpeg</button>
    <?= form_close(); ?>
  </div> -->
</div>
<div id="dialog"></div>
<hr>
<div class="table-responsive">
  <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th class="column-title text-center">ID</th>
        <th class="column-title text-center">NIP</th>
        <th class="column-title text-center">NAMA PEGAWAI</th>
        <th class="column-title text-center">STATUS</th>
        <th class="column-title text-center">Opsi</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if ($dtpegawai) {
        foreach ($dtpegawai as $row) { ?>
          <tr>
            <td><?= $row->id_pegawai; ?></td>
            <td><?= $row->nip; ?></td>
            <td><?= $row->nama_pegawai; ?></td>
            <td><?= $row->nama_status; ?></td>
            <td class="text-center">
              <button type="button" class="btn btn-warning btn-xs" id="edit" onclick="Editpeg('<?= $row->id_pegawai; ?>')">Edit</button>
            </td>
          </tr>
      <?php   }
      } ?>
    </tbody>
  </table>
</div>
<script>
  function Editpeg(id) {
    if (id) {
      var link = '<?= base_url('setting/formpeg/'); ?>' + id;
    } else {
      var link = '<?= base_url('setting/formpeg'); ?>';
    }
    $.ajax({
      url: link,
      type: "POST",
      data: "",
      success: function(data, textStatus, jqXHR) {
        $('#dialog').html(data);
        $("#dialogpegawai").modal();
      },
      error: function(jqXHR, status, error) {
        console.log(status + ": " + error);
      }
    });
  }
</script>