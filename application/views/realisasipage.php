<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="dialog"> </div>

<div class="table-responsive">
    <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">#</th>
                <th class="column-title text-center">Tahun</th>
                <th class="column-title text-center">Unit Kerja</th>
                <th class="column-title text-center">Nama Pelatihan</th>
                <th class="column-title text-center">Uraian</th>
                <th class="column-title text-center">Peserta</th>
                <th class="column-title text-center">Status</th>
                <th class="column-title text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $i = 0;
        if ($result){
            foreach ($result as $row){
                $idplt = $row['idplt'];
                $i++; ?>
            <tr>
                <td><?=$i;?></td>
                <td><?=$row['tahun'];?></td>
                <td><?=$row['nama_unit_kerja'];?></td>
                <td><?=$row['nmplt'];?></td>
                <td><?=$row['uraian'];?></td>
                <td class="text-center">
                    <?php echo $row['jmlpeserta']; ?>                                      
                </td>
                <td><?=$row['status'];?></td>
                <td class="text-center">
                    <?php 
                        echo anchor(base_url().'peserta/daftar/'.$row['idplt'].'/3', 'Peserta', array('class'=>'btn btn-primary btn-xs', 'title'=>'Update Peserta'));
                        echo '<a href="javascript:void(0);" class="btn btn-info btn-xs" onclick="resume('.$idplt.');">Resume</a>';
                        if ($this->modpelatihan->getjadwal($row['idplt'], 2)) {
                            echo '<a href="'.base_url().'realisasi/updstat/'.$row['idplt'].'" class="btn btn-warning btn-xs" onclick="return confirm(\'Yakin.. Semua data sudah benar?\')">Selesai</a>';
                        }
                    ?>
                    
                </td>
            </tr>
        <?php } }?>
        </tbody>
    </table>
</div>

<script>
    
    function resume(vidplt) {
        //var vidplt = $(this).val();
        $.ajax({
            url: "realisasi/formrealisasi",
            type: "POST",
            data: {idplt:vidplt},
            success: function(data, textStatus, jqXHR) {
                $('#dialog').html(data);
                $("#dialogjadwal").modal();
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error);
            }
        });
    };
           

    //$(document).ready(function() {
     //   $('.input-daterange input').each(function() {
    //    $(this).datepicker('clearDates');
   // });
    //});
 
    
</script>
