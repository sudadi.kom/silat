<?php defined('BASEPATH') or exit('No direct access script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($onoff){
    $on = 'active';
    $off = 'noActive';
} else {
    $off = 'active';
    $on = 'noActive';
}

echo form_open($action, 'id="formonoff" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="form-group">
    <label for="onoff" class="col-sm-2 control-label text-right">Akses Usulan </label>
    <div class="col-sm-2">
        <div class="input-group">
            <div id="onoff_radio" class="btn-group">
                <a class="btn btn-success btn-sm <?=$on;?>" data-toggle="onoff" data-title="1">Buka</a>
                <a class="btn btn-danger btn-sm <?=$off;?>" data-toggle="onoff" data-title="0">Tutup</a>
            </div>
            <input type="hidden" name="onoff" id="onoff" value="<?=$onoff;?>">
        </div>
    </div>
    <div class="col-sm-2">
        <button type="submit" class="btn btn-primary" name="btn-save" id="btn-submit"><i class="fa fa-save"></i> Simpan</button>
    </div>
</div>
<?=form_close(); ?>

<script>
$(document).ready(function(){
    $("#onoff_radio a").on('click', function(){
    var selected = $(this).data('title');
    var toggle = $(this).data('toggle');
    $('#'+toggle).prop('value', selected);
    $('a[data-toggle="'+toggle+'"]').not('[data-title="'+selected+'"]').removeClass('active').addClass('noActive');
    $('a[data-toggle="'+toggle+'"][data-title="'+selected+'"]').removeClass('noActive').addClass('active');
    })
});
</script>