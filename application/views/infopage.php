<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="panel panel-info">
    <div class="panel-heading">
        Tips
    </div>
    <div class="panel-body">
        Untuk akses melalui PC ataupun perangkat bergerak/mobile (HP, Tablet) pastikan anda menggunakan Internet Browser terbaru </br>
        (Firefox / Chrome / Opera).
    </div>
</div> 
<div class="panel panel-info">
<!--  <div class="panel-heading">
    Chart Realisasi Pelatihan
  </div>-->
  <div class="panel-body center-block">
      <h3 class="text-gray text-center">Realisasi Pelatihan Tahun <?=date("Y");?></h3>
    <canvas id="chartRealisasi" style="max-height: 40rem !important; max-width: 80rem !important;" class="center-block"></canvas>
  </div>
</div>


<script src="https://cdn.rso.go.id/chartjs/2.8.0/Chart.min.js"></script>
<script>
  $(function () {
    var violet = '#DF99CA',
        red = '#F0404C',
        green = '#7CF29C',
        blue = '#4680ff';
        
    var ctx1 = $("canvas").get(0).getContext("2d");
    var gradient1 = ctx1.createLinearGradient(150,0,150,400);
    gradient1.addColorStop(0, 'rgba(255,0,0,0.8)');
    gradient1.addColorStop(1, 'rgba(255,0,0,0)');

    var gradient2 = ctx1.createLinearGradient(150,0,150,400);
    gradient2.addColorStop(0, 'rgba(255,153,0,0.8)');
    gradient2.addColorStop(1, 'rgba(255,204,0,0)');
    
    var gradient3 = ctx1.createLinearGradient(150,0,150,400);
    gradient3.addColorStop(0, 'rgba(51,153,51,0.8)');
    gradient3.addColorStop(1, 'rgba(102,204,51,0)');
        
    // CHART KUNJUNGAN
    var realisasi = <?=$realisasi;?>;
    console.log(realisasi);
    var CHARTREALISASI = $('#chartRealisasi');
    var myLineChart = new Chart(CHARTREALISASI, {
        type: 'line',
        options: {
            legend: {labels:{fontColor:"#777", fontSize: 12}},
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        color: '#fff'
                    }
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        min: 0
                    },
                    gridLines: {
                        color: '#fff'
                    }
                }]
            },
            tooltips: {
                mode: 'nearest',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }
        },
        data: {
            labels: realisasi.bln,
            datasets: [
                {
                  label: "Realisasi",
                  fill: true,
                  lineTension: 0.3,
                  backgroundColor: gradient2,
                  borderColor: 'rgba(255,0,0,0.9)',
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  borderWidth: 2,
                  pointBorderColor: gradient2,
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 2,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: gradient2,
                  pointHoverBorderColor: "rgba(220,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: realisasi.jml,
                  spanGaps: false
                }
            ]
        }
    });
  })
   </script>  