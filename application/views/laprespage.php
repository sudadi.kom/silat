<?php defined('BASEPATH') or exit ('No direct access script allowed')

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <?=form_open($action, 'id="formlapjadwal" class="form-vertical form-label-left" data-parsley-validate'); ?>
    <div class="form-group">
      <label class="control-label col-sm-2 col-xs-12 text-right" for="periode"><h4>Periode :</h4></label>
        <div class="col-md-4">
          <div id="periode" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
    <i class="fa fa-calendar"></i>&nbsp;
    <span></span> <i class="fa fa-caret-down"></i>
</div>
          <?php
          //echo form_input('periode', date("Y/m/d")."-".date("Y/m/d"), 'id="periode" class="form-control" required');
          echo form_input(['name'=>'start','id'=>'start','type'=>"hidden",'required'=>'required']);
          echo form_input(['name'=>'end','id'=>'end','type'=>"hidden",'required'=>'required']);
          ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <button type="submit" class="btn btn-success"> Tampil <i class="fa fa-eye"></i></button>
            <?=anchor_popup(base_url('laporan/cetaklapresume/').$start.'/'.$end, 'Cetak <i class="fa fa-print"></i>', array('class'=>'btn btn-warning'));
            echo anchor(base_url('laporan/export_excel/').$start.'/'.$end, 'Export <i class="fa fa-file-excel-o"></i>', array('class'=>'btn btn-dark'));?>
        </div>
    </div>
    <?=form_close();?>
</div>
<br/>
<div class="separator"></div>

<div class="row">
    <div class="table-responsive">
        <table id="xtables" class="table table-striped table-bordered jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title text-center">#</th>
                    <th class="column-title text-center">Nama</th>
                    <th class="column-title text-center">Status Peg.</th>
                    <th class="column-title text-center">Unit Kerja</th>
                    <th class="column-title text-center">Usulan Pelatihan</th>
                    <th class="column-title text-center">JPL</th>
                    <th class="column-title text-center">Jadwal</th>
                    <th class="column-title text-center">Tempat</th>
                    <th class="column-title text-center">Total JPL</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $i = 0;
            if ($result){
                foreach ($result as $row){
                    $i++; 
                    $x=0;
                    $detail = $this->modlaporan->getresdetail($start, $end, $row['id_pegawai']);
                    foreach ($detail as $dtl) {
                        $x++; ?>
                        <tr>
                            <?php if ($x==1) {?>
                            <td <?=$row['sparow'] > 1 ? 'rowspan='.$row['sparow'] :'';?>><?=$i;?></td>
                            <td <?=$row['sparow'] > 1 ? 'rowspan='.$row['sparow'] :'';?>><?=$row['nama_pegawai'];?></td>
                            <td <?=$row['sparow'] > 1 ? 'rowspan='.$row['sparow'] :'';?>><?=$row['nama_status'];?></td>
                            <td <?=$row['sparow'] > 1 ? 'rowspan='.$row['sparow'] :'';?>><?=$row['nama_unit_kerja'];?></td>
                            <?php }?>
                            <td><?=$dtl['nmplt'];?></td>
                            <td><?=$dtl['jpl'];?></td>
                            <td><?=$dtl['mulai'].' <b>s/d</b> '.$dtl['selesai'];?></td>
                            <td><?=$dtl['tempat'];?></td>
                            <?php if ($x==1) {?>
                            <td <?=$row['sparow'] > 1 ? 'rowspan='.$row['sparow'] :'';?>>
                                <?php echo $row['total'];?>                                      
                            </td>
                            <?php }?>
                        </tr>
            <?php   } 
                } 
            } ?>
            </tbody>
        </table>

    </div>
</div>

<script>
  $(function() {

    var start = moment("<?=$start;?>");
    var end = moment("<?=$end;?>");

    function cb(start, end) {
        $('#periode span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#start').val(start.format('YYYY-MM-DD'));
        $('#end').val(end.format('YYYY-MM-DD'));
    }
 
    $('#periode').daterangepicker({
      "showDropdowns": true,
      "minYear": 2017,
      ranges: {
          'Sekarang': [moment(), moment()],
          'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
          'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
          'Tahun Ini': [moment().startOf('year'), moment().endOf('year')],
          'Tahun Kemarin': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
      },
      "locale": {
         "format": "YYYY/MM/DD",
       },
      "alwaysShowCalendars": false,
      "startDate": "<?=date('Y/m/d');?>",
      "endDate": "<?=date('Y/m/d');?>"
    }, cb )
    
    cb(start, end);
    console.log('<?=$start;?>');
    });
</script>