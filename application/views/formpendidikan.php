<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($edit && !empty($dtdidik)){
    $id = $dtdidik[0]->id;
    $idpeg = $dtdidik[0]->pegawai_id;
    $jenjangid = $dtdidik[0]->jenjang_id;
    $tipe = $dtdidik[0]->tipe;
    $univid = $dtdidik[0]->univ_id;
    $fak = $dtdidik[0]->fak;
    $thnmsk = $dtdidik[0]->thn_masuk;
    $thnlls = $dtdidik[0]->thn_lulus;
} else {
  $jenjangid = $tipe = $univid = $fak = $thnmsk = $thnlls = '';
}
echo form_open($action, 'id="formpendidikan" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogpendidikan" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$judul;?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="id">ID</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'id', 'type'=>'text', 'value'=>$id ,'class'=>'form-control col-sm-12 col-xs-12', 'readonly'=>'readonly');
            echo form_input($attribut);?>
          </div>
        </div> 
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="">Jenjang</label>
          <div class="col-sm-4 col-xs-12">
            <?php 
            $option['']='Jenjang Pendidikan';
            if ($refjenjang) {
              foreach($refjenjang as $value){
                $option[$value->id_jenjang]=$value->nm_jenjang;
              }
            }
            echo form_dropdown('jenjangid', $option, $jenjangid, 'class="form-control col-sm-12 col-xs-12" required');?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="tipe">Jenis</label>
          <div class="col-sm-3 col-xs-12">
            <?php  
            unset($option);
            $option[0]='Pendidikan Dasar';
            $option[1]='Pendidikan lanjut';
            echo form_dropdown('tipe', $option, $tipe, 'class="form-control col-sm-12 col-xs-12" id="tipe" required');?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="ptns">PTN/S</label>
          <div class="col-sm-4 col-xs-12">
            <?php 
            unset($option);
            $option['']='Perguruan Tinggi';
            foreach($refuniv as $value){
              $option[$value->id_univ]=$value->nm_univ;
            }
            echo form_dropdown('univid', $option, $univid, 'id="ptns" class="form-control col-sm-12 col-xs-12" required');?>
          </div>  
          <label class="control-label col-sm-2 col-xs-12" for="fak">Fakultas</label>
          <div class="col-sm-4 col-xs-12">
            <?php $attribut = array('name'=>'fak', 'type'=>'text', 'id'=>'fak', 'value'=>$fak ,'class'=>'form-control col-sm-12 col-xs-12');
            echo form_input($attribut);?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="thnmsk">Tahun Masuk</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'thnmsk', 'type'=>'number', 'id'=>'thnlls', 'value'=>$thnmsk, 'placeholder'=>"YYYY", 'min'=>"1980",'class'=>'form-control col-sm-12 col-xs-12');
            echo form_input($attribut);
            ?>
          </div> 
          <label class="control-label col-sm-2 col-xs-12" for="thnlls">Tahun Lulus</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'thnlls', 'type'=>'number', 'id'=>'thnlls', 'value'=>$thnlls, 'placeholder'=>"YYYY", 'min'=>"1980",'class'=>'form-control col-sm-12 col-xs-12');
            echo form_input($attribut);
            echo form_hidden('idpeg', $idpeg);
            ?>
          </div> 
        </div>
        <div class="modal-footer">
          <?=form_input(['name'=>'edit', 'type'=>'hidden', 'value'=>$edit]);?>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <?php echo form_button(array('name'=>'simpan','type'=>'submit', 'class'=>'btn btn-success', 'id' =>'submit', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>