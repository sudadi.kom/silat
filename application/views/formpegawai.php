<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($edit && !empty($dtpeg)){
    $idpeg = $dtpeg[0]->id_pegawai;
    $namapeg = $dtpeg[0]->nama_pegawai; 
    $nip = $dtpeg[0]->nip;
    $statpeg = $dtpeg[0]->status_pegawai;
    $unitker = $dtpeg[0]->id_unit_kerja;
    $dikdas = $dtpeg[0]->didik_id;
} else {
  $namapeg = $nip = $statpeg = $unitker = $dikdas = '';
}
echo form_open($action, 'id="formsatker" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogpegawai" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Pegawai</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="idpeg">ID</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'idpeg', 'type'=>'text', 'value'=>$idpeg ,'class'=>'form-control col-sm-12 col-xs-12', 'readonly'=>'readonly');
            echo form_input($attribut);?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="nip">NIP</label>
          <div class="col-sm-3 col-xs-12">
            <?php $attribut = array('name'=>'nip', 'type'=>'text', 'value'=>$nip,'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
            echo form_input($attribut);?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="namapeg">Nama</label>
          <div class="col-sm-5 col-xs-12">
            <?php $attribut = array('name'=>'namapeg', 'type'=>'text', 'value'=>$namapeg,'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
            echo form_input($attribut);?>
          </div>
        </div>
        <div class="form-group">          
          <label class="control-label col-sm-2 col-xs-12" for="dikdas">Pendidikan</label>
          <div class="col-sm-3 col-xs-12">
            <?php 
            $option['']='-Pendidikan Dasar-';
            foreach($refjenjang as $value){
              $option[$value->id_jenjang]=$value->nm_jenjang;
            }
            echo form_dropdown('dikdas', $option, $dikdas, 'class="form-control col-sm-12 col-xs-12" required');?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="unitker">Satker</label>
          <div class="col-sm-4 col-xs-12">
            <?php 
            unset($option);
            $option['']='-Satuan Kerja-';
            foreach($refunit as $value){
              $option[$value->id_unit_kerja]=$value->nama_unit_kerja;
            }
            echo form_dropdown('unitker', $option, $unitker, 'class="form-control col-sm-12 col-xs-12" required');?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="status">Status</label>
          <div class="col-sm-3 col-xs-12">
            <?php  
            unset($option);
            $option['']='-Pilih Status-';
            foreach($refstatus as $value){
              $option[$value->id_status_pegawai]=$value->nama_status;
            }
            echo form_dropdown('statpeg', $option, $statpeg, 'class="form-control col-sm-12 col-xs-12" id="statpeg" required');?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <?=form_input(['name'=>'edit', 'type'=>'hidden', 'value'=>$edit]);?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo form_button(array('name'=>'simpan','type'=>'submit', 'class'=>'btn btn-success', 'id' =>'submit', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>
