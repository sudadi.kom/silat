<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$tahun = $pelatihan = $uraian = $idplt = '';
if ($plt) {
    $idplt = $plt->idplt;
    $tahun = $plt->tahun;
    $pelatihan = $plt->nmplt;
    $uraian = $plt->uraian;
}
echo form_open($action, 'id="formusulan" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogusulan" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Usulan Pelatihan</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-sm-2 col-xs-12" for="tahun">Tahun</label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?php 
                        $selisih = date('Y') - 2017;
                        $option[''] = '-Tahun-';
                        for ($i = 0; $i <= $selisih; $i++){
                            $thn = 2018 + $i;
                            $option[$thn] = $thn;
                        }
                        echo form_dropdown('tahun', $option, $tahun, 'class="form-control col-sm-12 col-xs-12" id="tahun" required');?>
                    </div>
                    <label class="control-label col-sm-2 col-xs-12" for="pelatihan">Pelatihan</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php $attribut = array('name'=>'pelatihan', 'value'=>$pelatihan,'type'=>'text', 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
                        echo form_input($attribut);?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2 col-xs-12" for="uraian">Uraian </label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php $attribut = array('name'=>'uraian', 'value'=>$uraian,'class'=>'form-control col-sm-12 col-xs-12', 'rows'=>'1', 'required'=>'required');
                        echo form_textarea($attribut);?>
                    </div>
                    <?php echo form_hidden('idplt', $idplt);?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <?php echo form_button(array('type'=>'submit', 'class'=>'btn btn-success', 'id' =>'submit', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
            
 
