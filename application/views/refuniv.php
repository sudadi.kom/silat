<?php defined('BASEPATH') or exit('No dirrect script aceess allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-sm-6 text-right">
    <button type="button" class="btn btn-primary" id="tambah" onclick="Edituniv(false);"><i class="fa fa-plus"></i> Tambah PT</button>
  </div>
</div>
<div id="dialog"></div>
<hr>
<div class="table-responsive">
    <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">ID</th>
                <th class="column-title text-center">UNIVERSITAS</th>
                <th class="column-title text-center">LOKASI</th>
                <th class="column-title text-center">STATUS</th>
                <th class="column-title text-center">OPSI</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        if ($dtrefuniv){
            foreach ($dtrefuniv as $row){ ?>
            <tr>
                <td><?=$row->id_univ;?></td>
                <td><?=$row->nm_univ;?></td>
                <td><?=$row->lok_univ;?></td>
                <td><?=($row->status) ? 'Aktif':'Tidak Aktif';?></td>
                <td class="text-center">
                   <button type="button" class="btn btn-warning btn-xs" id="edit" onclick="Edituniv('<?=$row->id_univ;?>')">Edit</button>
                </td>
            </tr>
        <?php   }
        } ?>
        </tbody>
    </table>
</div>
<script>
  function Edituniv(id) {
    var uri = '<?=uri_string();?>';
    console.log(uri);
    if (id){
      var link = '<?=base_url('setting/formuniv/');?>'+id;
    } else {
      var link = '<?=base_url('setting/formuniv/');?>';
    }
    $.ajax({
      url: link,
      type: "POST",
      data: {uri:uri},
      success: function(data, textStatus, jqXHR) {
          $('#dialog').html(data);
          $("#dialogrefuniv").modal();
      },
      error: function(jqXHR, status, error) {
          console.log(status + ": " + error);
      }
  });
}
</script>