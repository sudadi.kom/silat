<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=Resume_pelatihan.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
<html>
<head>
<style>
table{
      border-collapse: collapse;
      font-family: arial;
      font-size: 12;
      color:#5E5B5C;
      width:100%;
      border-collapse: collapse;
      border : 1px solid #e3e3e3;
    }
 
    thead th{
      text-align: left;
      padding: 10px;
      background-color: #dedede;
    }
 
    tbody td{
      border: 1px solid #e3e3e3;
      padding: 5px;
    }
    
    
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    }
</style>
</head>
<body>
<table  border: 1px solid black;>
    <thead>
        <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Status Peg.</th>
            <th>Unit Kerja</th>
            <th>Usulan Pelatihan</th>
            <th>JPL</th>
            <th>Jadwal</th>
            <th>Tempat</th>
            <th>Total JPL</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $i = 0;
        if ($result){
            foreach ($result as $row){
                $i++; 
                $detail = $this->modlaporan->getresdetail($start, $end, $row['id_pegawai'])?>
             <?php $x=0;
            foreach ($detail as $dtl) {
                $x++?>
            <tr>
                <?php if ($x==1) {?>
                <td rowspan="<?=$row['sparow'];?>"><?=$i;?></td>
                <td rowspan="<?=$row['sparow'];?>"><?=$row['nama_pegawai'];?></td>
                <td rowspan="<?=$row['sparow'];?>"><?=$row['nama_status'];?></td>
                <td rowspan="<?=$row['sparow'];?>"><?=$row['nama_unit_kerja'];?></td>
                <?php }?>
                <td><?=$dtl['nmplt'];?></td>
                <td><?=$dtl['jpl'];?></td>
                <td><?=$dtl['mulai'].' s/d '.$dtl['selesai'];?></td>
                <td><?=$dtl['tempat'];?></td>
                <?php if ($x==1) {?>
                <td rowspan="<?=$row['sparow'];?>">
                    <?php echo $row['total']; ?>                                      
                </td>
                <?php }?>
            </tr>
        <?php } } } ?>
    </tbody>
</table>
    
</body>
</html>