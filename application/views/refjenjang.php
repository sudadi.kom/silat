<?php defined('BASEPATH') or exit('No dirrect script aceess allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-sm-6 text-right">
    <button type="button" class="btn btn-primary" id="tambah" onclick="Editjenjang(false);"><i class="fa fa-plus"></i> Tambah Pendidikan</button>
  </div>
</div>
<div id="dialog"></div>
<hr>
<div class="table-responsive">
    <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">ID</th>
                <th class="column-title text-center">PENDIDIKAN</th>
                <th class="column-title text-center">KETERANGAN</th>
                <th class="column-title text-center">STATUS</th>
                <th class="column-title text-center">OPSI</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        if ($dtrefjenjang){
            foreach ($dtrefjenjang as $row){ ?>
            <tr>
                <td><?=$row->id_jenjang;?></td>
                <td><?=$row->nm_jenjang;?></td>
                <td><?=$row->ket;?></td>
                <td><?=($row->status) ? 'Aktif':'Tidak Aktif';?></td>
                <td class="text-center">
                   <button type="button" class="btn btn-warning btn-xs" id="edit" onclick="Editjenjang('<?=$row->id_jenjang;?>')">Edit</button>
                </td>
            </tr>
        <?php   }
        } ?>
        </tbody>
    </table>
</div>
<script>
  function Editjenjang(id) {
    if (id){
      var link = '<?=base_url('setting/formjenjang/');?>'+id;
    } else {
      var link = '<?=base_url('setting/formjenjang');?>';
    }
    $.ajax({
      url: link,
      type: "POST",
      data: "",
      success: function(data, textStatus, jqXHR) {
          $('#dialog').html(data);
          $("#dialogjenjang").modal();
      },
      error: function(jqXHR, status, error) {
          console.log(status + ": " + error);
      }
  });
}
</script>