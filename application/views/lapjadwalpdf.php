<!DOCTYPE html>
<html lang="en">

  <head>
   <?php header('Content-Type: application/pdf');?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Silat - <?=$judul;?> </title>
    <!-- Custom styles for this template -->
    <style>
    div {
    margin: auto;
    border: 1px solid gray;
    padding: 8px;
    }
    h3 {
        text-align: center;
        text-transform: uppercase;
    }
        
    #outtable{
      padding: 5px;
      border:1px solid #e3e3e3;
      width:100%;
      border-radius: 5px;
      
    }
 
    table{
      border-collapse: collapse;
      font-family: arial;
      font-size: 12;
      color:#5E5B5C;
      width:100%;
      border-collapse: collapse;
    }
 
    thead th{
      text-align: left;
      padding: 10px;
      background-color: #dedede;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 5px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    }

</style>

</head>

<body>
    <h3> <?=$judul;?> </h3>
    <h4 style="text-align: center">Tahun <?=$thn;?></h4>
    <div id="outtable">
      <table>
          <thead>
              <tr class="headings">
                  <th class="column-title text-center">#</th>
                    <th class="column-title text-center">Tahun</th>
                    <th class="column-title text-center">Unit Kerja</th>
                    <th class="column-title text-center">Nama Pelatihan</th>
                    <th class="column-title text-center">JPL</th>
                    <th class="column-title text-center">Jadwal</th>
                    <th class="column-title text-center">Tempat</th>
                    <th class="column-title text-center">Peserta</th>
              </tr>
          </thead>
          <tbody>
          <?php 
          $i = 0;
          if ($result){
              foreach ($result as $row){
                  $i++; ?>
                <tr>
                    <td><?=$i;?></td>
                    <td><?=$row['tahun'];?></td>
                    <td><?=$row['nama_unit_kerja'];?></td>
                    <td><?=$row['nmplt'];?></td>
                    <td><?=$row['jpl'];?></td>
                    <td><?=$row['mulai'].' s/d '.$row['selesai'];?></td>
                    <td><?=$row['tempat'];?></td>
                    <td class="text-center">
                      <?php echo $row['jmlpeserta']; ?>                                      
                    </td>
                </tr>
          <?php }} ?>
          </tbody>
      </table>
    </div>
</body>
</html>
