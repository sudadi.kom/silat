<?php defined('BASEPATH') or exit('No dirrect script aceess allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($onoff) {?>
<button type="button" class="btn btn-primary" id="tambah" onclick="usulan();"><i class="fa fa-plus"></i> Tambah Usulan</button>
<?php } ?>
<div id="dialog"> </div>
<hr />
<div class="table-responsive">
    <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">#</th>
                <th class="column-title text-center">Tahun</th>
                <th class="column-title text-center">Nama Pelatihan</th>
                <th class="column-title text-center">Uraian</th>
                <th class="column-title text-center">Peserta</th>
                <th class="column-title text-center">Status</th>
                <th class="column-title text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $i = 0;
        if ($result){
            foreach ($result as $row){
                $i++; ?>
            <tr>
                <td><?=$i;?></td>
                <td><?=$row['tahun'];?></td>
                <td><?=$row['nmplt'];?></td>
                <td><?=$row['uraian'];?></td>
                <td class="text-center">
                    <?php 
                    echo $row['jmlpeserta'].'&nbsp&nbsp';
                    if ($row['idstatus'] == 0 || $row['idstatus'] == 1) { 
                        echo anchor('peserta/daftar/'.$row['idplt'].'/false', '<i class="fa fa-user-plus"></i>', 
                                array('class'=>'btn-info btn-sm', 'title'=>'Edit Peserta'));
                    }
                    ?>                    
                </td>
                <td class="text-center"><?='<i class="bg-green">'.$row['status'].'</i>';?></td>
                <td class="text-center">
                    <?php
                    if ($row['jmlpeserta'] == 0) {
                        echo anchor('usulan/hapus/'.$row['idplt'], 'Hapus', 
                            array('class'=>'btn btn-danger btn-xs', 'title'=>'Hapus Usulan', 'onClick'=>"return confirm('Yakin menghapus data tersebut?')")).' ';
                    } else if ($row['idstatus'] == 0) { 
                        echo anchor('usulan/kirim/'.$row['idplt'], 'Kirim', 
                          array('class'=>'btn btn-info btn-xs', 'title'=>'Kirim Usulan', 'onClick'=>"return confirm('Yakin.. Data yang anda masukkan sudah benar.?')"));
                    }
                    if ($row['idstatus'] == 0 || $row['idstatus'] == 1) {?>
                    <button type="button" class="btn btn-warning btn-xs" id="edit" onclick="usulan('<?=$row['idplt'];?>');"><i class="fa fa-edit"></i> Edit</button>
                        <?php } ?>
                </td>
            </tr>
        <?php   }
        } ?>
        </tbody>
    </table>
</div>
<script>
/* must apply only after HTML has loaded */
$(document).ready(function () {
    
    //$("#tambah").click(function(){
     //   $("#dialogusulan").modal();
   // });
    
    
//    $("#tambah").on("click", function() {
//        $.ajax({
//            url: "usulan/formusulan",
//            type: "POST",
//            data: "",
//            success: function(data, textStatus, jqXHR) {
//                $('#dialog').html(data);
//                $("#dialogusulan").modal();
//            },
//            error: function(jqXHR, status, error) {
//                console.log(status + ": " + error);
//            }
//        });
//    });
          
    //$("#submitForm").on('click', function() {
     //   $("#formusulan").submit();
   // });
});

    function usulan(id) {
        if (id) {
           var url = "usulan/formusulan/"+id;
        } else {
           var url = "usulan/formusulan";
        }
        $.ajax({
            url: url,
            type: "POST",
            data: "",
            success: function(data, textStatus, jqXHR) {
                $('#dialog').html(data);
                $("#dialogusulan").modal();
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error);
            }
        });
    }
</script>