<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo form_open($action, 'id="formrencana" class="form-vertical form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogjadwal" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?=$title;?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12" for="tempat">Tempat</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php $attribut = array('name'=>'tempat', 'type'=>'text', 'value'=>$tempat, 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
                            echo form_input($attribut);?>
                        </div>
                    </div> <br/><br/>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12" for="daterange">Tanggal Pelaksanaan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="input-prepend input-group">
                              <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                              <input style="width: 200px" name="daterange" id="reservation" class="form-control" value="<?=$mulai.' - '.$selesai;?>" type="text" required="required">
                            </div>
                        </div>
                        <input type="hidden" name="idplt" value="<?=$idplt;?>">
                        <input type="hidden" name="mulai" id="mulai" value="<?=$mulai;?>">
                        <input type="hidden" name="selesai" id="selesai" value="<?=$selesai;?>">
                    </div>
                    <br/>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12" for="jpl">Jam Pelajaran</label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <?php $attribut = array('name'=>'jpl', 'type'=>'number', 'value'=>$jpl, 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
                            echo form_input($attribut);?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>&nbsp;
                <button type="submit" class="btn btn-warning" onclick="return confirm('Yakin.. Data tersebut sudah benar..?')">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
<script>
    //$(document).ready(function() {
     //   $('.input-daterange input').each(function() {
    //    $(this).datepicker('clearDates');
   // });
    //});
    
$(document).ready(function(){ 
    $('input[name="daterange"]').daterangepicker(
    {
        locale: {
          format: 'YYYY-MM-DD'
        }
    }, 
    function(start, end, label) {
        
        $('#mulai').val(start.format('YYYY-MM-DD'));
        $('#selesai').val(end.format('YYYY-MM-DD'));
        //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD')+ ' '+$('#selesai').val());
    });
 });
  
</script> 