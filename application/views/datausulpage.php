<?php defined('BASEPATH') or exit ('No direct access script allowed')

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="table-responsive">
    <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">#</th>
                <th class="column-title text-center">Tahun</th>
                <th class="column-title text-center">Unit Kerja</th>
                <th class="column-title text-center">Nama Pelatihan</th>
                <th class="column-title text-center">Uraian</th>
                <th class="column-title text-center">Peserta</th>
                <th class="column-title text-center">Status</th>
                <th class="column-title text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $i = 0;
        if ($result){
            foreach ($result as $row){
                $i++; ?>
            <tr>
                <td><?=$i;?></td>
                <td><?=$row['tahun'];?></td>
                <td><?=$row['nama_unit_kerja'];?></td>
                <td><?=$row['nmplt'];?></td>
                <td><?=$row['uraian'];?></td>
                <td class="text-center">
                    <?php echo $row['jmlpeserta']; 
                    echo anchor('peserta/daftar/'.$row['idplt'].'/1', '<i class="fa fa-user-plus"></i>', 
                                array('class'=>'btn-info btn-sm', 'title'=>'Edit Peserta'));
                    ?>                                      
                </td>
                <td><?=$row['status'];?></td>
                <td class="text-center">
                    <?php 
                        echo anchor(base_url().'datausulan/proses/'.$row['idplt'], 'Rekap', array('class'=>'btn-success btn-xs', 'title'=>'Proses Pengajuan'));
                    } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>