<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Datatables -->
<script type="text/javascript" src="https://cdn.rso.go.id/datatables/bundle-1.10.18-bs3/datatables.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?=base_url('assets/js/custom.min.js');?>"></script>
<script src="<?=base_url('assets/js/auto.js');?>"></script>
<script src="https://cdn.rso.go.id/toastr/2.1.4/toastr.min.js"></script>
<!-- <script src="<?=base_url('assets/vendors/select2/dist/js/select2.min.js');?>" type="text/javascript"></script> -->
<script src="https://cdn.rso.go.id/select2/4.0.10/select2.min.js"></script>
<script src="https://cdn.rso.go.id/datepicker/1.9.0/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script>
$(document).ready(function() {
    $('#dtables').DataTable({
       "dom": 'lBfrtip',
		 "buttons": [
            {
                extend: 'collection',
                text: 'Export',
                className: 'btn btn-dark',
                buttons: [
                    'excel'
                ]
            }
        ]
    });
    
    $('#otables').DataTable();

    $(".js-select2").select2();
        
    <?php if($this->session->flashdata('success')){ ?>
        toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php } if($this->session->flashdata('error')){  ?>
        toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php } if($this->session->flashdata('warning')){  ?>
        toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php } if($this->session->flashdata('info')){  ?>
        toastr.info("<?php echo $this->session->flashdata('info'); ?>");
<?php } ?>
});

function getmnbyunit() {
	var x = document.getElementById("myidunit").value;
    window.location = "aksesmn?idunit="+x;
}

function toggleFullScreen() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
  }  
}
</script>
