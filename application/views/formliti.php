<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($edit && !empty($dtliti)){
    $id = $dtliti[0]->id;
    $idpeg = $dtliti[0]->pegawai_id;
    $judulliti = $dtliti[0]->judul;
    $link = $dtliti[0]->link;
    $data = $dtliti[0]->data;
    $thn = $dtliti[0]->thn;
} else {
  $judulliti = $link = $data = $thn = '';
}
echo form_open($action, 'id="formpenelitian" class="form-horizontal form-label-left" data-parsley-validate  method="post" enctype="multipart/form-data" '); ?>
<div class="modal fade" id="dialogliti" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$judul;?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="id">ID</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'id', 'type'=>'text', 'value'=>$id ,'class'=>'form-control col-sm-12 col-xs-12', 'readonly'=>'readonly');
            echo form_input($attribut);?>
          </div>
        </div> 
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="judul">Judul</label>
          <div class="col-sm-4 col-xs-12">
            <?php $attribut = array('name'=>'judul', 'type'=>'text', 'id'=>'judul', 'value'=>$judulliti ,'class'=>'form-control col-sm-12 col-xs-12'); 
            echo form_input($attribut); ?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="link">Publikasi/Link</label>
          <div class="col-sm-3 col-xs-12">
           <?php $attribut = array('name'=>'link', 'type'=>'text', 'id'=>'link', 'value'=>$link ,'class'=>'form-control col-sm-12 col-xs-12');
           echo form_input($attribut); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="data">Data-File</label>
          <div class="col-sm-4 col-xs-12">
            <?php $attribut = array('name'=>'dtfile', 'type'=>'file', 'id'=>'dtfile','class'=>'form-control col-sm-12 col-xs-12');
            echo form_input($attribut); ?>
            <p class="help-block">File .pdf</p>
          </div>  
          <label class="control-label col-sm-2 col-xs-12" for="thn">Tahun</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'thn', 'type'=>'number', 'id'=>'thn', 'value'=>$thn ,'class'=>'form-control col-sm-12 col-xs-12');
            echo form_input($attribut);
            echo form_hidden('idpeg', $idpeg);
            ?>
          </div> 
        </div>
        <div class="modal-footer">
          <?=form_input(['name'=>'edit', 'type'=>'hidden', 'value'=>$edit]);?>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <?php echo form_button(array('name'=>'simpan','type'=>'submit', 'class'=>'btn btn-success', 'id' =>'submit', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>