<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($edit && !empty($dtjenjang)){
    $idjenjang = $dtjenjang[0]->id_jenjang;
    $nmjenjang = $dtjenjang[0]->nm_jenjang;
    $ket = $dtjenjang[0]->ket;
    $stat = $dtjenjang[0]->status;
} else {
  $nmjenjang = $stat = $ket = '';
}
echo form_open($action, 'id="formrefdidik" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogjenjang" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$judul;?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="idjenjang">ID</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'idjenjang','id'=>'idjenjang', 'type'=>'text', 'value'=>$idjenjang ,'class'=>'form-control col-sm-12 col-xs-12', 'readonly'=>'readonly');
            echo form_input($attribut);?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="nmjenjang">Jenjang</label>
          <div class="col-sm-6 col-xs-12">
            <?php $attribut = array('name'=>'nmjenjang','id'=>'nmjenjang', 'type'=>'text', 'value'=>$nmjenjang,'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
            echo form_input($attribut);?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="stat">Status</label>
          <div class="col-sm-2 col-xs-12">
            <?php 
            $option[1]='Aktif';
            $option[0]='Tidak Aktif';
            echo form_dropdown('stat', $option, $stat, 'class="form-control col-sm-6 col-xs-12" id="stat" required');?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="ket">Keterangan</label>
          <div class="col-sm-6 col-xs-12">
            <?php $attribut = array('name'=>'ket', 'type'=>'text', 'value'=>$ket,'class'=>'form-control col-sm-12 col-xs-12');
            echo form_input($attribut);?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <?=form_input(['name'=>'edit', 'type'=>'hidden', 'value'=>$edit]);?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo form_button(array('name'=>'simpan','type'=>'submit', 'class'=>'btn btn-success', 'id' =>'submit', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>
