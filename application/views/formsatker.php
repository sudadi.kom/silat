<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($edit && !empty($dtsat)){
    $idsat = $dtsat[0]->id_unit_kerja;
    $namasat = $dtsat[0]->nama_unit_kerja; 
} else {
  $namasat = '';
}
echo form_open($action, 'id="formsatker" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogsatker" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Form Satker</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-sm-2 col-xs-12" for="tahun">ID</label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?php $attribut = array('name'=>'idsat', 'type'=>'text', 'value'=>$idsat ,'class'=>'form-control col-sm-12 col-xs-12', 'readonly'=>'readonly');
                        echo form_input($attribut);?>
                    </div>
                    <label class="control-label col-sm-2 col-xs-12" for="pelatihan">Nama Satker</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php $attribut = array('name'=>'namasat', 'type'=>'text', 'value'=>$namasat,'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
                        echo form_input($attribut);?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?=form_input(['name'=>'edit', 'type'=>'hidden', 'value'=>$edit]);?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <?php echo form_button(array('name'=>'simpan','type'=>'submit', 'class'=>'btn btn-success', 'id' =>'submit', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
