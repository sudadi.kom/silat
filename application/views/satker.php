<?php defined('BASEPATH') or exit('No dirrect script aceess allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<button type="button" class="btn btn-primary" id="tambah" onclick="Editsat(false);"><i class="fa fa-plus"></i> Tambah Satker</button>
<div id="dialog"> </div>
<hr />
<div class="table-responsive">
    <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">#</th>
                <th class="column-title text-center">ID</th>
                <th class="column-title text-center">Nama Satker</th>
                <th class="column-title text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $i = 0;
        if ($dtsatker){
            foreach ($dtsatker as $row){
                $i++; ?>
            <tr>
                <td><?=$i;?></td>
                <td><?=$row->id_unit_kerja;?></td>
                <td><?=$row->nama_unit_kerja;?></td>
                <td class="text-center">
                  <button type="button" class="btn btn-warning btn-xs" id="edit" onclick="Editsat('<?=$row->id_unit_kerja;?>')">Edit</button>
                </td>
            </tr>
        <?php   }
        } ?>
        </tbody>
    </table>
</div>


<script>
  
  function Editsat(id) {
    if (id){
      var link = '<?=base_url('setting/formsat/');?>'+id;
    } else {
      var link = '<?=base_url('setting/formsat');?>';
    }
    $.ajax({
      url: link,
      type: "POST",
      data: "",
      success: function(data, textStatus, jqXHR) {
          $('#dialog').html(data);
          $("#dialogsatker").modal();
      },
      error: function(jqXHR, status, error) {
          console.log(status + ": " + error);
      }
  });
}
</script>