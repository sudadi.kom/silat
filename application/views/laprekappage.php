<?php defined('BASEPATH') or exit ('No direct access script allowed')

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <?=form_open($action, 'id="formlaprekap" class="form-vertical form-label-left" data-parsley-validate'); ?>
    <div class="form-group">
        <label class="control-label col-sm-2 col-xs-12" for="tahun">Tahun</label>
        <div class="col-md-2 col-sm-2 col-xs-12">
            <?php 
            $selisih = date('Y') - 2017;
            $option[''] = '-Tahun-';
            for ($i = 0; $i <= $selisih; $i++){
                $thn = 2018 + $i;
                $option[$thn] = $thn;
            }
            echo form_dropdown('tahun', $option, $tahun, 'class="form-control col-sm-12 col-xs-12" id="tahun" required');?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <button type="submit" class="btn btn-success"> Tampil <i class="fa fa-eye"></i></button>
            <?=anchor_popup(base_url('laporan/cetaklaprekap/').$tahun, 'Cetak <i class="fa fa-print"></i>', array('class'=>'btn btn-info'));?>
        </div>
    </div>
    <?=form_close();?>
</div>
<br/>
<div class="separator"></div>
<div class="row">
    <div class="table-responsive">
        <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title text-center">#</th>
                    <th class="column-title text-center">Tahun</th>
                    <th class="column-title text-center">Unit Kerja</th>
                    <th class="column-title text-center">Nama Pelatihan</th>
                    <th class="column-title text-center">Uraian</th>
                    <th class="column-title text-center">Peserta</th>

                </tr>
            </thead>
            <tbody>
            <?php 
            $i = 0;
            if ($result){
                foreach ($result as $row){
                    $i++; ?>
                <tr>
                    <td><?=$i;?></td>
                    <td><?=$row['tahun'];?></td>
                    <td><?=$row['nama_unit_kerja'];?></td>
                    <td><?=$row['nmplt'];?></td>
                    <td><?=$row['uraian'];?></td>
                    <td class="text-center">
                        <?php echo $row['jmlpeserta']; ?>                                      
                    </td>
                </tr>
            <?php }} ?>
            </tbody>
        </table>

    </div>
</div>