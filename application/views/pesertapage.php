<?php defined('BASEPATH') or exit('No direct access script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


echo form_open($action, 'id="formusulan" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="form-group">
    <label class="control-label col-sm-2 col-xs-12" for="peserta">Peserta</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php 
            $option=NULL;
            $option['']='--Pilih Nama Peserta--';
            $peserta = $this->modref->getpegawai();
            foreach ($peserta as $value) {
                $option[$value['id_pegawai']] = $value['nama_pegawai'];
            }
            echo form_dropdown('peserta', $option, '', 'class="js-select2 form-control col-sm-12" required');
        ?>
    </div>
    <?php 
    echo form_hidden('idplt', $idplt);
    echo form_hidden('susulan', $susulan);
    ?>
</div>
<div class="ln_solid"></div>
<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-4">
        <button type="submit" class="btn btn-success"> Simpan <i class="fa fa-save"></i></button>
        <a href="<?=$backurl;?>" class="btn btn-warning"> Selesai <i class="fa fa-undo"></i></a>
    </div>
</div>
<?=form_close(); ?>

<hr/>
<div class="table-responsive">
    <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">#</th>
                <th class="column-title text-center">NIP</th>
                <th class="column-title text-center">Nama Peserta</th>
                <th class="column-title text-center">Stat. Peg.</th>
                <th class="column-title text-center">Kompetensi</th>
                <th class="column-title text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $i = 0;
        if ($result){
            foreach ($result as $row){
                if ($row['sesuai']==0){
                    $sesuai='Tidak Sesuai';
                }else{
                    $sesuai='Sesuai';
                }
                $i++; ?>
            <tr>
                <td><?=$i;?></td>
                <td><?=$row['nip'];?></td>
                <td><?=$row['nama_pegawai'];?></td>
                <td><?=$row['nama_status'];?></td>
                <td><?=$sesuai;?></td>
                <td class="text-center">
                    <?php
                        echo anchor(base_url().'peserta/hapus/'.$row['idpeserta'].'/'.$row['idplt'], 'Hapus', 
                            array('class'=>'btn-danger btn-sm', 'title'=>'Hapus Peserta', 'onClick'=>"return confirm('Yakin menghapus data tersebut?')"));
                        if ($this->session->userdata('iduser')==1){
                            echo ' '.anchor(base_url().'peserta/updstatus/'.$row['idpeserta'].'/'.$row['idplt'].'/'.$row['sesuai'].'/'.$susulan, 'Kompetensi', 
                                array('class'=>'btn-info btn-sm', 'title'=>'Kesesuaian Kompetensi'));
                        }
                    ?>
                </td>
            </tr>
        <?php   }
        } ?>
        </tbody>
    </table>
</div>
