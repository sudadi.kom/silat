<?php defined('BASEPATH') or exit('No direct access script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo form_open($action, 'id="formpegawai" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="form-group hidden-print">
  <label class="control-label col-sm-2 col-xs-12" for="peserta">Pegawai</label>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <?php 
      $option=NULL;
      $option['']='--Pilih Nama Pegawai--';
      if ($pegawai) {
        foreach ($pegawai as $value) {
          $option[$value['id_pegawai']] = $value['nama_pegawai'];
        }
      }
      echo form_dropdown('idpeg', $option, $idpeg, 'class="js-select2 form-control col-sm-12" required');
    ?>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-12">
    <button type="submit" class="btn btn-success"> Tampil <i class="fa fa-eye"></i></button>

  </div>
</div>
<div class="ln_solid hidden-print"></div>
<?=form_close();?> 

<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
  <?php if (!$dtpegawai) { ?>
  <div class="profile_img">
    <div id="crop-avatar">
      <img class="img-responsive avatar-view" src="<?=base_url('assets/images/user.png');?>" alt="Avatar" title="Change the avatar">
    </div>
  </div>
  <?php } else { ?>
  <div class="profile_img">
    <div id="crop-avatar">
      <img class="img-responsive avatar-view" src="<?=base_url('assets/images/user.png');?>" alt="Avatar" title="Change the avatar">
    </div>
  </div>
  <h5><?=$dtpegawai[0]->nama_pegawai;?></h5>
  <ul class="list-unstyled user_data">
    <li><i class="fa fa-credit-card user-profile-icon"></i> NIP. <?=$dtpegawai[0]->nip;?></li>
    <li><i class="fa fa-object-group user-profile-icon"></i> <?=$dtpegawai[0]->nama_unit_kerja;?></li>
  </ul>
  <a class="btn btn-success hidden-print"><i class="fa fa-edit m-right-xs"></i> Edit Pegawai</a>
  <?php } ?>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
  <div class="profile_title">
    <div class="col-md-12">
      <h2>Penelitian</h2>
    </div>
  </div>
  <table class="data table table-striped no-margin">
    <thead>
      <tr>
        <th>Judul</th>
        <th>Publikasi/Link</th>
        <th>Data</th>
        <th>Tahun</th>
        <th class="hidden-print">Opsi</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($dtpenelitian) { 
      foreach ($dtpenelitian as $val) { ?>
      <tr>
        <td><?=$val->judul;?></td>
        <td><?=$val->link;?></td>
        <td><?="<a href=".base_url('penelitian/download/').$val->data.">".$val->data."</a>";?></td>
        <td><?=$val->thn;?></td>
        <td class="hidden-print">
          <!--<button class="btn btn-warning btn-xs" type="button" onclick="editliti('<?=$idpeg;?>','<?=$val->id;?>');" title="Edit"><i class="fa fa-edit"></i></button>-->
          <a href="<?=base_url('penelitian/hapusliti/').$idpeg.'/'.$val->id;?>" class="btn btn-danger btn-xs" type="button" onclick="return confirm('Yakin menghapus data tersebut?')" title="Hapus"><i class="fa fa-trash"></i></a>
        </td>
      </tr>
      <?php } 
      } ?>
    </tbody>
  </table>
  <div class="col-xs-12">
    <?php if ($dtpegawai) { ?>
    <button class="btn btn-default hidden-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
    <button class="btn btn-primary pull-right hidden-print" onclick="editliti('<?=$idpeg;?>', false);"><i class="fa fa-plus-circle"></i> Tambah</button>
    <?php } ?>
  </div>
</div>
<div id="dialog"></div>
<script>
  function editliti(idpeg, idliti) {
    if (idliti){
      var link = '<?=base_url('Penelitian/formpenelitian/');?>'+idpeg+'/'+idliti;
    } else {
      var link = '<?=base_url('Penelitian/formpenelitian/');?>'+idpeg;
    }
    $.ajax({
      url: link,
      type: "POST",
      success: function(data, textStatus, jqXHR) {
          $('#dialog').html(data);
          $("#dialogliti").modal();
      },
      error: function(jqXHR, status, error) {
          console.log(status + ": " + error);
      }
  });
}
</script>