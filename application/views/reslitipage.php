<?php defined('BASEPATH') or exit ('No direct access script allowed')

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <?=form_open($action, 'id="formresliti" class="form-vertical form-label-left" data-parsley-validate'); ?>
    <div class="form-group">
      <label class="control-label col-sm-2 col-xs-12" for="tahun">Tahun</label>
        <div class="col-md-2 col-sm-2 col-xs-12">
            <?php 
            $selisih = date('Y') - 2017;
            $option[0] = 'Semua';
            for ($i = 1; $i <= $selisih; $i++){
                $thn = 2017 + $i;
                $option[$thn] = $thn;
            }
            echo form_dropdown('tahun', $option, $tahun, 'class="form-control col-sm-12 col-xs-12" id="tahun" required');?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <button type="submit" class="btn btn-success"> Tampil <i class="fa fa-eye"></i></button>
            <!-- <?=anchor_popup(base_url('laporan/cetaklaprekap/').$tahun, 'Cetak <i class="fa fa-print"></i>', array('class'=>'btn btn-info'));?> -->
        </div>
    </div>
    <?=form_close();?>
</div>
<br/>
<div class="separator"></div>

<div class="row">
    <div class="table-responsive">
        <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title text-center">#</th>
                    <th class="column-title text-center">Nama Pegawai</th>
                    <th class="column-title text-center">Judul</th>
                    <th class="column-title text-center">Publikasi/Link</th>
                    <th class="column-title text-center">Data/File</th>
                    <th class="column-title text-center">Tahun</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $i = 0;
            if ($dtpenelitian){
              foreach ($dtpenelitian as $row){
                $i++; ?>
              <tr>
                <td><?=$i;?></td>
                <td><?=$row->nama_pegawai;?></td>
                <td><?=$row->judul;?></td>
                <td><?=$row->link;?></td>
                <td><?="<a href=".base_url('penelitian/download/').$row->data.">".$row->data."</a>";?></td>
                <td><?=$row->thn;?></td>
              </tr>    
                    
            <?php 
                } 
            } ?>
            </tbody>
        </table>

    </div>
</div>

<script>
  $(function() {

    var start = moment("<?=$start;?>");
    var end = moment("<?=$end;?>");

    function cb(start, end) {
        $('#periode span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#start').val(start.format('YYYY-MM-DD'));
        $('#end').val(end.format('YYYY-MM-DD'));
    }
 
    $('#periode').daterangepicker({
      "showDropdowns": true,
      "minYear": 2017,
      ranges: {
          'Sekarang': [moment(), moment()],
          'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
          'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
          'Tahun Ini': [moment().startOf('year'), moment().endOf('year')],
          'Tahun Kemarin': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
      },
      "locale": {
         "format": "YYYY/MM/DD",
       },
      "alwaysShowCalendars": false,
      "startDate": "<?=date('Y/m/d');?>",
      "endDate": "<?=date('Y/m/d');?>"
    }, cb )
    
    cb(start, end);
    console.log('<?=$start;?>');
    });
</script>