<?php defined('BASEPATH') or exit ('No direct access script allowed')

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <?=form_open($action, 'id="formresdidik" class="form-horizontal form-label-left" data-parsley-validate'); ?>
    <div class="form-group col-md-4">
      <label class="control-label col-md-6" for="thnMasuk">Tahun Masuk</label>      
      <div class="col-md-6">
        <input type="number" name="thnMasuk" placeholder="YYYY" min="1980" class="form-control" id="thnMasuk">
      </div>
    </div>
    <div class="form-group col-md-4">
      <label class="control-label col-md-6" for="thnLulus">Tahun Lulus</label>      
      <div class="col-md-6">
        <input type="number" name="thnLulus" placeholder="YYYY" min="1980" class="form-control" id="thnLulus" >
      </div>
    </div>
    <div class="form-group col-md-4">
      <label class="control-label col-md-5" for="status">Status</label>      
      <div class="col-md-7">
        <?php 
        $option[2] = 'Semua';
        $option[0] = 'Belum Lulus';
        $option[1] = 'Sudah Lulus';
        echo form_dropdown('status', $option, 2, 'class="form-control" id="status" ');?>      
      </div>
    </div>
    
  <div class="form-group col-sm-12 block text-center">
    <button type="submit" class="btn btn-success"> Tampil <i class="fa fa-eye"></i></button>
    <!-- <?=anchor_popup(base_url('laporan/cetaklaprekap/').$tahun, 'Cetak <i class="fa fa-print"></i>', array('class'=>'btn btn-info'));?> -->
  </div>
  <?=form_close();?>
</div>
<br/>
<div class="separator"></div>

<div class="row">
    <div class="table-responsive">
        <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title text-center">#</th>
                    <th class="column-title text-center">Nama Pegawai</th>
                    <th class="column-title text-center">Jenjang</th>
                    <th class="column-title text-center">Fak./Jurusan</th>
                    <th class="column-title text-center">Perguruan Tinggi</th>
                    <th class="column-title text-center">Th. Masuk</th>
                    <th class="column-title text-center">Th. Lulus</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $i = 0;
            if ($dtpendidikan){
              foreach ($dtpendidikan as $val){
                $i++; ?>
              <tr>
                <td><?=$i;?></td>
                <td><?=$val->nama_pegawai;?></td>
                <td><?=$val->nm_jenjang;?></td>
                <td><?=$val->fak;?></td>
                <td><?=$val->nm_univ;?></td>
                <td><?=$val->thn_masuk;?></td>
                <td><?=$val->thn_lulus;?></td>
              </tr>   
                    
            <?php 
                } 
            } ?>
            </tbody>
        </table>

    </div>
</div>

