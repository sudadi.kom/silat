<?php defined('BASEPATH') or exit('No direct access script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


echo form_open($action, 'id="formusulan" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="col-sm-6">
    <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12" for="username">Nama Pengguna</label>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <?php $attribut = array('name'=>'username', 'id'=>'username', 'type'=>'text', 'class'=>'form-control', 'required'=>'');
                echo form_input($attribut);?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12" for="realname">Nama Lengkap</label>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <?php $attribut = array('name'=>'realname', 'id'=>'realname', 'type'=>'text', 'class'=>'form-control', 'required'=>'');
                echo form_input($attribut);?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12" for="password">Password</label>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <?php $attribut = array('name'=>'password', 'id'=>'password', 'type'=>'text', 'class'=>'form-control', 'required'=>'');
                echo form_input($attribut);?>
        </div>
    </div>
    </div>
<div class="col-sm-6">
    <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12" for="unit">Unit Kerja</label>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <?php 
                $option = NULL;
                $option['']='--Pilih Unit Kerja--';
                $unit = $this->modref->getunit();
                foreach ($unit as $key => $value) {
                    $option[$value->id_unit_kerja] = $value->nama_unit_kerja;
                }
                echo form_dropdown('unit', $option, '', 'class="js-select2 form-control col-xs-12 col-sm-8 col-lg-12" required id="unit"');
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12" for="level">Akses Level</label>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <?php 
                $option=NULL;
                $option['']='--Pilih Level Akses--';
                $option[1] = 'Admin';
                $option[2] = 'Pengguna';
                echo form_dropdown('level', $option, '', 'class="form-control col-sm-12" required id="level"');
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4 col-xs-12" for=""></label>
        <div class="col-md-8 col-sm-8 col-xs-12">
            
        </div>
    </div>    
</div>

<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-4">
        <button type="submit" class="btn btn-success"> Simpan <i class="fa fa-save"></i></button>
        <a href="" class="btn btn-warning"> Selesai <i class="fa fa-undo"></i></a>
    </div>
</div>
<?=form_close(); ?>

<hr/>
<div class="table-responsive">
    <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">#</th>
                <th class="column-title text-center">User</th>
                <th class="column-title text-center">Nama</th>
                <th class="column-title text-center">Satker</th>
                <th class="column-title text-center">Status</th>
                <th class="column-title text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $i = 0 ;
        if ($result){
            foreach ($result as $row){
                if ($row['status']==0){
                    $status='Tidak Aktif';
                }else{
                    $status='Aktif';
                }
                $i++; ?>
            <tr>
                <td><?=$i;?></td>
                <td><?=$row['username'];?></td>
                <td><?=$row['nama'];?></td>
                <td><?=$row['nama_unit_kerja'];?></td>
                <td><?=$status;?></td>
                <td class="text-center">
                    <?php
                        echo anchor(base_url().'setting/hapususer/'.$row['iduser'], 'Hapus', 
                            array('class'=>'btn btn-danger btn-xs', 'title'=>'Hapus Pengguna', 'onClick'=>"return confirm('Yakin menghapus data tersebut?')"));
                        if ($this->session->userdata('iduser')==1){
                            echo ' '.anchor(base_url().'setting/updstatususer/'.$row['iduser'].'/'.$row['status'], 'Status', 
                                array('class'=>'btn btn-info btn-xs', 'title'=>'Ubah Status'));
                            echo ' '.anchor(base_url().'setting/resetpass/'.$row['iduser'], 'Reset Sandi', 
                                array('class'=>'btn btn-info btn-xs', 'title'=>'Reset Sandi','onClick'=>"return confirm('Yakin mereset Sandi untuk user tersebut?')"));
                        }
                    ?>
                </td>
            </tr>
        <?php   }
        } ?>
        </tbody>
    </table>
</div>