<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo form_open($action, 'id="formtolak" class="form-vertical form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogtolak" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Alasan Tidak Disetujui </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-sm-2 col-xs-12" for="ket">Keterangan </label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php $attribut = array('name'=>'ket', 'class'=>'form-control col-sm-12 col-xs-12', 'rows'=>'2', 'required'=>'required');
                        echo form_textarea($attribut);?>
                    </div>
                </div>
                <br/><br/>
            </div>
            <div class="modal-footer">
                <?php echo form_hidden('idplt', $idplt);?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>&nbsp;
                <button type="submit" class="btn btn-warning" onclick="return confirm('Yakin.. Usulan pelatihan tersebut di tolak..?')">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
            
 
