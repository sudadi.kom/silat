<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div id="dialog"> </div>
<div class="table-responsive">
    <table id="otables" class="table table-striped table-bordered jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title text-center">#</th>
                <th class="column-title text-center">Tahun</th>
                <th class="column-title text-center">Unit Kerja</th>
                <th class="column-title text-center">Nama Pelatihan</th>
                <th class="column-title text-center">Uraian</th>
                <th class="column-title text-center">Peserta</th>
                <th class="column-title text-center">Status</th>
                <th class="column-title text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $i = 0;
        if ($result){
            foreach ($result as $row){
                $i++; 
                $idplt = $row['idplt']?>
            <tr>
                <td><?=$i;?></td>
                <td><?=$row['tahun'];?></td>
                <td><?=$row['nama_unit_kerja'];?></td>
                <td><?=$row['nmplt'];?></td>
                <td><?=$row['uraian'];?></td>
                <td class="text-center">
                    <?php echo $row['jmlpeserta']; 
                    echo anchor('peserta/daftar/'.$row['idplt'].'/1', '<i class="fa fa-user-plus"></i>', 
                                array('class'=>'btn-info btn-sm', 'title'=>'Edit Peserta'));?>                                      
                </td>
                <td><?=$row['status'];?></td>
                <td class="text-center">
                    <?php 
                        echo anchor(base_url().'persetujuan/proses/'.$row['idplt'].'/3', 'Disetujui', array('class'=>'btn btn-success btn-xs', 'title'=>'Usulan Disetujui'));
                        //echo anchor('', 'Ditolak', array('class'=>'btn btn-danger btn-xs', 'title'=>'Tidak Disetujui', 'onclick'=>"tolak(\');"));
                    ?>    
                    <a href="javascript:void(0);" class="btn btn-danger btn-xs" onclick="tolak(<?php echo $idplt;?>);">Ditolak</a>    
                </td>
            </tr>
        <?php } }?>
        </tbody>
    </table>
</div>

<script>
    
    function tolak(vidplt) {
        //var vidplt = $(this).val();
        $.ajax({
            url: "persetujuan/formtolak",
            type: "POST",
            data: {idplt:vidplt},
            success: function(data, textStatus, jqXHR) {
                $('#dialog').html(data);
                $("#dialogtolak").modal();
            },
            error: function(jqXHR, status, error) {
                console.log(status + ": " + error);
            }
        });
    };
          
    
</script>