<?php defined('BASEPATH') or exit('No direct script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($edit && !empty($dtuniv)){
    $iduniv = $dtuniv[0]->id_univ;
    $nmuniv = $dtuniv[0]->nm_univ;
    $lokasi = $dtuniv[0]->lok_univ;
    $statuniv = $dtuniv[0]->status;
} else {
  $nmuniv = $statuniv = $lokasi = '';
}
echo form_open($action, 'id="formrefuniv" class="form-horizontal form-label-left" data-parsley-validate'); ?>
<div class="modal fade" id="dialogrefuniv" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$judul;?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="iduniv">ID</label>
          <div class="col-sm-2 col-xs-12">
            <?php $attribut = array('name'=>'iduniv', 'type'=>'text', 'value'=>$iduniv ,'class'=>'form-control col-sm-12 col-xs-12', 'readonly'=>'readonly');
            echo form_input($attribut);?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="nmuniv">Universitas</label>
          <div class="col-sm-6 col-xs-12">
            <?php $attribut = array('name'=>'nmuniv', 'type'=>'text', 'value'=>$nmuniv,'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
            echo form_input($attribut);?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2 col-xs-12" for="statuniv">Status</label>
          <div class="col-sm-2 col-xs-12">
            <?php 
            $option[1]='Aktif';
            $option[0]='Tidak Aktif';
            echo form_dropdown('statuniv', $option, $statuniv, 'class="form-control col-sm-6 col-xs-12" id="statdidik" required');?>
          </div>
          <label class="control-label col-sm-2 col-xs-12" for="lokasi">Lokasi</label>
          <div class="col-sm-6 col-xs-12">
            <?php $attribut = array('name'=>'lokasi', 'type'=>'text', 'value'=>$lokasi,'class'=>'form-control col-sm-12 col-xs-12');
            echo form_input($attribut);?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <?php 
        echo form_input(['name'=>'edit', 'type'=>'hidden', 'value'=>$edit]);
        echo form_input(['name'=>'uri', 'type'=>'hidden', 'value'=>$uri]);
        ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo form_button(array('name'=>'simpan','type'=>'submit', 'class'=>'btn btn-success', 'id' =>'submit', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>
