<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo form_open($action, 'class="form-horizontal form-label-left" data-parsley-validate methode="POST"');?>
    <div class="form-group">
        <div class="col-sm-12 col-xs-12">
            <label class="control-label col-sm-5 col-xs-12" for="jml">Password Lama</label>
            <div class="col-sm-7 col-xs-12">
                <?php $attribut = array('name'=>'oldpass', 'type'=>'password', 'class'=>'form-control', 'required'=>'');
                echo form_input($attribut);?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12 col-xs-12">
            <label class="control-label col-sm-5 col-xs-12" for="jml">Password Baru</label>
            <div class="col-sm-7 col-xs-12">
                <?php $attribut = array('name'=>'newpass1', 'type'=>'password', 'class'=>'form-control', 'required'=>'');
                echo form_input($attribut);?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12 col-xs-12">
            <label class="control-label col-sm-5 col-xs-12" for="jml">Ulang Password Baru</label>
            <div class="col-sm-7 col-xs-12">
                <?php $attribut = array('name'=>'newpass2', 'type'=>'password', 'class'=>'form-control', 'required'=>'');
                echo form_input($attribut);?>
            </div>
        </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-4">
            <?php echo form_button(array('type'=>'submit', 'class'=>'btn btn-success', 'content'=>'Simpan'));
                echo form_button(array('type'=>'reset', 'class'=>'btn btn-warning', 'content'=>'Batal'));?>
        </div>
    </div>
<?=form_close();?>