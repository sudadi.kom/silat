<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class MY_Controller extends CI_Controller {
    
    public $aksesmenu=[];
    
    public function __construct() {
        parent::__construct();
        $this->load->model('moduser');
    }
    
    public function __aksesmenu(){
        $akses = $this->moduser->akses($this->session->userdata('idunit'));
        if ($akses){
            foreach ($akses as $row){
                $menu['idmenu'][] = $row['idmenu'];
                $menu['menu'][] = $row['menu'];
                $menu['link'][] = $row['link'];
                $menu['icon'][] = $row['icon'];
                $menu['sub'][]  = $row['sub'];
            }
        }
        return $menu;       
    }
    
    public function __cekakses() {
        $aksesmenu = $this->moduser->akses($this->session->userdata('idunit'));
        if ($this->session->userdata('iduser') !=='1' && !in_array('2', $this->aksesmenu)){
            return ;  
        }   
    }
}
?>