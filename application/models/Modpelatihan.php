<?php defined('BASEPATH') or exit('No dirrect script aceess allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Modpelatihan extends CI_Model {
    
  function __construct() {
    parent::__construct();

  }    

  function getusulan($thn, $idunit, $idstatus) {
    $this->db->join('refstatus', 'tusulan.idstatus = refstatus.idstatus');
    $this->db->join('refunitkerja', 'tusulan.id_unit_kerja = refunitkerja.id_unit_kerja');
    if ($idunit == 'all') { //diklit - admin
      $this->db->where('tusulan.idstatus', $idstatus);
    } else {
      $this->db->where('tusulan.id_unit_kerja', $idunit);
    }
    if ($thn) {
      $this->db->where('tahun', $thn);
    }
    return $this->db->get('tusulan')->result_array();
  }

  function getnmplt($idplt) {
    $this->db->select('idplt, tahun, nmplt, uraian');
    return $this->db->get_where('tusulan', ['idplt'=>$idplt])->row();
  }

  function updstat($idplt, $stat, $ket=null) {
    $this->db->set('ket', $ket);
    $this->db->set('idstatus', $stat);
    $this->db->set('idplt', $idplt);
    $this->db->insert('thistori');
    return $this->db->affected_rows();
  }

  function getpeserta($idplt) {
    $this->db->select('tpeserta.*, refstatpeg.nama_status, refpegawai.nip, refpegawai.nama_pegawai');
    $this->db->join('refpegawai', 'tpeserta.id_pegawai= refpegawai.id_pegawai', 'left');
    $this->db->join('refstatpeg', 'refpegawai.status_pegawai = refstatpeg.id_status_pegawai', 'left');
    $this->db->where('idplt', $idplt);
    return $this->db->get('tpeserta')->result_array();
  }

  function savepeserta($idplt, $idpeg, $susulan, $status) {
    if ($susulan) {
      $susulan = 1;
    }
    $this->db->insert('tpeserta', array('idplt'=>$idplt, 'id_pegawai'=>$idpeg, 'susulan'=>$susulan, 'status'=>$status));
    if ($this->db->affected_rows() > 0) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function getjadwal($idplt, $idjenis) {
    $this->db->where('idplt', $idplt);
    $this->db->where('idjenis', $idjenis);
    return $this->db->get('tjadwal')->row();
  }

  function getonoff() {
    return $this->db->get('tonoff', 1)->row(1)->onoff;
  }
  
  function getpendidikan($where=null){
    if ($where){
      $this->db->where($where);
    }
    return $this->db->get('tpendidikan')->result();
  }

  function getdatadidik($where=null) {
    if ($where) {
      $this->db->where($where);
    }
    $this->db->select('id, nama_pegawai, tipe,fak,thn_masuk,thn_lulus,nm_jenjang,nm_univ');
    $this->db->join('refjenjang', 'refjenjang.id_jenjang=tpendidikan.jenjang_id');
    $this->db->join('refuniv', 'refuniv.id_univ=tpendidikan.univ_id');
    $this->db->join('refpegawai', 'refpegawai.id_pegawai=tpendidikan.pegawai_id');
    return $this->db->get('tpendidikan')->result();
  }
  
  function savependidikan() {
    $id = $this->input->post('id');
    $jenjangid = $this->input->post('jenjangid');
    $tipe = $this->input->post('tipe');
    $univid = $this->input->post('univid');
    $fak = $this->input->post('fak');
    $thnmsk = $this->input->post('thnmsk');
    $thnlls = $this->input->post('thnlls');
    $idpeg = $this->input->post('idpeg');
    $edit = $this->input->post('edit');
    if ($edit) {
      $this->db->update('tpendidikan', ['pegawai_id'=>$idpeg, 'univ_id'=>$univid, 'fak'=>$fak, 'thn_masuk'=>$thnmsk, 'thn_lulus'=>$thnlls, 'jenjang_id'=>$jenjangid, 'tipe'=>$tipe], "id='$id'");
    } else {
      $this->db->insert('tpendidikan', ['pegawai_id'=>$idpeg, 'univ_id'=>$univid, 'fak'=>$fak, 'thn_masuk'=>$thnmsk, 'thn_lulus'=>$thnlls, 'jenjang_id'=>$jenjangid, 'tipe'=>$tipe]);
    }
    return $this->db->affected_rows();
  }
  
  function getpenelitian($where=null){
    if ($where){
      $this->db->where($where);
    }
    return $this->db->get('tpenelitian')->result();
  }
  
  function getdataliti($where=null) {
    if ($where) {
      $this->db->where($where);
    }
    $this->db->select('tpenelitian.*, refpegawai.nama_pegawai');
    $this->db->join('refpegawai', 'refpegawai.id_pegawai=tpenelitian.pegawai_id');
    return $this->db->get('tpenelitian')->result();
  }
  
  function savepenelitian() {
    $id = $this->input->post('id');
    $judul = $this->input->post('judul');
    $link = $this->input->post('link');
    $thn = $this->input->post('thn');
    $idpeg = $this->input->post('idpeg');
    $edit = $this->input->post('edit');
    $file = '';
    //var_dump($this->input->post('dtfile')); die();
    if (isset($_FILES['dtfile']['name']) && !empty($_FILES['dtfile']['name'])) {
      $this->load->model('Modupload');
      if(!($file = $this->Modupload->uploadFile('dtfile'))) {
        $this->session->set_flashdata('warning', $this->upload->display_errors());
      }
    }
    $field = ['pegawai_id'=>$idpeg, 'judul'=>$judul, 'link'=>$link, 'thn'=>$thn, 'data'=>$file];
    if ($edit) {
      $this->db->update('tpenelitian', $field, "id='$id'");
    } else {
      $this->db->insert('tpenelitian', $field);
    }
    return $this->db->affected_rows();
  }
  
  function getdatalatih($idpeg) {
    $this->db->select('nmplt, tahun, tempat, jpl');
    $this->db->join('tpeserta', 'tusulan.idplt=tpeserta.idplt');
    $this->db->join('tjadwal', 'tjadwal.idplt=tusulan.idplt');
    return $this->db->get_where('tusulan', ['tpeserta.id_pegawai'=>$idpeg, 'tusulan.idstatus'=>5, 'tjadwal.idjenis'=>2])->result();
  }
}
?>
