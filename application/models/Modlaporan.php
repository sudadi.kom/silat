<?php defined('BASEPATH') or exit('No dirrect script aceess allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Modlaporan extends CI_Model {
    
    function __construct() {
        parent::__construct();
        
    }  
 
    function getlaprekap($thn, $idstatus) {
        $this->db->join('refstatus', 'tusulan.idstatus = refstatus.idstatus')
        ->join('refunitkerja', 'tusulan.id_unit_kerja = refunitkerja.id_unit_kerja')
        ->join('thistori', 'thistori.idplt=tusulan.idplt');
        if ($idstatus == 4){
            $this->db->join('tjadwal', 'tusulan.idplt=tjadwal.idplt');
            $this->db->where('idjenis',1);
        }
        $this->db->where('thistori.idstatus', $idstatus);
        
        $this->db->where('tahun', $thn);
        return $this->db->get('tusulan')->result_array();
    }
    
    function getresume($start, $end) {
        $this->db->select('tpeserta.id_pegawai, count(tpeserta.idpeserta) as sparow, refpegawai.nama_pegawai, refunitkerja.nama_unit_kerja, 
                refstatpeg.nama_status, sum(tjadwal.jpl) as total')
                ->join('refpegawai', 'refpegawai.id_pegawai=tpeserta.id_pegawai')
                ->join('refstatpeg', 'refstatpeg.id_status_pegawai=refpegawai.status_pegawai')
                ->join('tusulan', 'tusulan.idplt=tpeserta.idplt')
                ->join('refunitkerja', 'tusulan.id_unit_kerja=refunitkerja.id_unit_kerja')
                ->join('tjadwal', 'tjadwal.idplt=tusulan.idplt');
        $where = [
            'tjadwal.idjenis'=>2,
            'tusulan.idstatus'=>5,
            'tjadwal.mulai >='=>$start,
            'tjadwal.mulai <='=>$end];

        $this->db->where($where);
        $this->db->group_by('tpeserta.id_pegawai')
                ->order_by('tusulan.id_unit_kerja');
            
        return $this->db->get('tpeserta')->result_array();
    }
    
    function getresdetail($start, $end, $idpeg) {
        $this->db->select('tusulan.nmplt, tpeserta.status, tjadwal.tempat, tjadwal.mulai, tjadwal.selesai, tjadwal.jpl')
                ->join('tusulan', 'tusulan.idplt=tpeserta.idplt')
                ->join('tjadwal', 'tjadwal.idplt=tusulan.idplt');
        $where = [
            'tjadwal.idjenis'=>2,
            'tusulan.idstatus'=>5,
            'tpeserta.id_pegawai'=>$idpeg,
            'tjadwal.mulai >='=>$start,
            'tjadwal.mulai <='=>$end];
        $this->db->where($where);
        return $this->db->get('tpeserta')->result_array();
    }
    function dashRealplt($where) {
      $this->db->select('count(*) as jml, DATE_FORMAT(mulai, "%Y-%m-01") as tgl, MONTHNAME(mulai) as bln');
      $data = $this->db->get_where('tjadwal', $where)->result();
      if (!empty($data)) {
      foreach ($data as $value) {
        $dataplt['jml'][]= $value->jml;
        $dataplt['bln'][]= $value->bln;
      } 
    }else {
      $dataplt = ['jlm'=>[0],'bln'=>['Januari']];
    }
    return $dataplt;
    }
}