<?php defined('BASEPATH') or exit('No direct script access allowed');

/** 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor
 * @property CI_DB_mysqli_driver $db 
 */

class Modref extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function getunit($idunit = null)
  {
    if ($idunit != null) {
      $this->db->where('id_unit_kerja', $idunit);
    }
    return $this->db->get('refunitkerja')->result();
  }

  function getstatusplt($idstatus)
  {
    $this->db->where('idstatus', $idstatus);
    return $this->db->get('refstatus')->row();
  }
  function getrefstatpeg($where)
  {
    return $this->db->get_where('refstatpeg', $where)->result();
  }

  function getdatasat($where)
  {
    return $this->db->get_where('refunitkerja', $where)->result();
  }

  function savedtsat()
  {
    $id = $this->input->post('idsat');
    $nmsat = $this->input->post('namasat');
    $edit = $this->input->post('edit');
    if (!empty($edit)) {
      $result = $this->db->update('refunitkerja', ['id_unit_kerja' => $id, 'nama_unit_kerja' => $nmsat], "id_unit_kerja='$id'");
    } else {
      $result = $this->db->insert('refunitkerja', ['id_unit_kerja' => $id, 'nama_unit_kerja' => $nmsat]);
    }
    return $result;
  }

  function getpegawai()
  {
    $this->db->where("status_pegawai=2 or status_pegawai=3 or status_pegawai=4 or status_pegawai=5 or status_pegawai=6");
    return $this->db->get('refpegawai')->result_array();
  }

  function getdatapeg($where)
  {
    $this->db->join('refstatpeg', 'refstatpeg.id_status_pegawai=refpegawai.status_pegawai');
    $this->db->join('refunitkerja', 'refunitkerja.id_unit_kerja=refpegawai.id_unit_kerja');
    return $this->db->get_where('refpegawai', $where)->result();
  }

  function savedatapeg()
  {
    $id = $this->input->post('idpeg');
    $nmpeg = $this->input->post('namapeg');
    $edit = $this->input->post('edit');
    $nip = $this->input->post('nip');
    $stat = $this->input->post('statpeg');
    $dikdas = $this->input->post('dikdas');
    $unitker = $this->input->post('unitker');
    if (!empty($edit)) {
      $result = $this->db->update('refpegawai', ['id_pegawai' => $id, 'nama_pegawai' => $nmpeg, 'nip' => $nip, 'status_pegawai' => $stat, 'didik_id' => $dikdas, 'id_unit_kerja' => $unitker], "id_pegawai='$id'");
    } else {
      $result = $this->db->insert('refpegawai',  ['id_pegawai' => $id, 'nama_pegawai' => $nmpeg, 'nip' => $nip, 'didik_id' => $dikdas, 'id_unit_kerja' => $unitker, 'status_pegawai' => $stat]);
    }
    return $result;
  }

  function syncdatapeg()
  {
    //$dbsimpeg = $this->load->database('dbsimpeg', TRUE);
    //echo json_encode($this->db->get('db_simpeg.info_status')->result());
    //die();
    $sql = 'INSERT INTO dbsilat.refpegawai (id_pegawai, badge, nip, nip_lama, no_kartu_pegawai, nama_pegawai, jenis_kelamin, agama, '
      . 'status_pegawai, alamat, id_status_jabatan, id_jabatan, id_unit_kerja, id_satuan_kerja, lokasi_kerja) SELECT id_pegawai, badge, '
      . 'nip, nip_lama, no_kartu_pegawai, nama_pegawai, jenis_kelamin, agama, status_pegawai, alamat, id_status_jabatan, id_jabatan, '
      . 'id_unit_kerja, id_satuan_kerja, lokasi_kerja from db_simpeg.tbl_data_pegawai WHERE id_pegawai NOT IN (SELECT id_pegawai FROM dbsilat.refpegawai)';
    $this->db->query($sql);
    return $this->db->affected_rows();
  }

  function getrefjenjang($where)
  {
    return $this->db->get_where('refjenjang', $where)->result();
  }

  function saverefjenjang()
  {
    $id = $this->input->post('idjenjang');
    $nmjenjang = $this->input->post('nmjenjang');
    $edit = $this->input->post('edit');
    $ket = $this->input->post('ket');
    $stat = $this->input->post('stat');
    if (!empty($edit)) {
      $result = $this->db->update('refjenjang', ['id_jenjang' => $id, 'nm_jenjang' => $nmjenjang, 'ket' => $ket, 'status' => $stat], "id_jenjang='$id'");
    } else {
      $result = $this->db->insert('refjenjang',  ['id_jenjang' => $id, 'nm_jenjang' => $nmjenjang, 'ket' => $ket, 'status' => $stat]);
    }
    return $result;
  }

  function getrefuniv($where)
  {
    return $this->db->get_where('refuniv', $where)->result();
  }

  function saverefuniv()
  {
    $id = $this->input->post('iduniv');
    $nmuniv = $this->input->post('nmuniv');
    $edit = $this->input->post('edit');
    $lokasi = $this->input->post('lokasi');
    $stat = $this->input->post('statuniv');
    if (!empty($edit)) {
      $result = $this->db->update('refuniv', ['id_univ' => $id, 'nm_univ' => $nmuniv, 'lok_univ' => $lokasi, 'status' => $stat], "id_univ='$id'");
    } else {
      $result = $this->db->insert('refuniv',  ['id_univ' => $id, 'nm_univ' => $nmuniv, 'lok_univ' => $lokasi, 'status' => $stat]);
    }
    return $result;
  }
}
