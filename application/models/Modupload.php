<?php defined('BASEPATH') or exit('No direct access script allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Modupload extends CI_Model {
    
  function __construct() {
    parent::__construct();

  }   
  
  public function uploadFile($field)
  {
    $config['upload_path']          = './upload/penelitian/';
    $config['allowed_types']        = 'pdf';
    $config['overwrite']			= true;
    $config['max_size']             = 10240; // 10MB
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;

    $this->load->library('upload', $config);
    if ($this->upload->do_upload($field)) {
      return $this->upload->data("file_name");
    } 
    
  }
  
}