-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 25 Sep 2019 pada 10.08
-- Versi Server: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsilat`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`dbadmin`@`localhost` FUNCTION `aksesnemenu` () RETURNS TINYINT(3) NO SQL
BEGIN
INSERT INTO takses (idmenu, active, iduser) (SELECT '27', '1', tuser.iduser FROM tuser);
RETURN 1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `refdirektorat`
--

CREATE TABLE `refdirektorat` (
  `id` int(1) NOT NULL,
  `direktorat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `refdirektorat`
--

INSERT INTO `refdirektorat` (`id`, `direktorat`) VALUES
(1, 'DIREKTUR UTAMA'),
(2, 'DIREKTUR MEDIK DAN KEPERAWATAN'),
(3, 'DIREKTUR UMUM, SDM DAN PENDIDIKAN'),
(4, 'DIREKTUR KEUANGAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `refjabatan`
--

CREATE TABLE `refjabatan` (
  `id_jabatan` int(50) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `refjabatan`
--

INSERT INTO `refjabatan` (`id_jabatan`, `nama_jabatan`, `level`) VALUES
(1, 'Dokter Pendidik Klinis Utama (JFT)', ''),
(2, 'Direktur Keuangan', ''),
(3, 'Dokter Madya (JFT)', ''),
(4, 'Dokter Pendidik Klinis Madya (JFT)', ''),
(5, 'Direktur Umum, SDM, dan Pendidikan', ''),
(6, 'Kepala Bidang Pelayanan Medik', ''),
(7, 'Dokter Gigi Madya (JFT)', ''),
(8, 'Perawat Madya (JFT)', ''),
(9, 'Direktur Medik dan Keperawatan', ''),
(10, 'Apoteker Madya (JFT)', ''),
(11, 'Kepala Bagian Sumber Daya Manusia', ''),
(12, 'Kabag Perbendaharaan dan Mobilisasi Dana', ''),
(13, 'Kepala Bagian Umum', ''),
(14, 'Psikolog Klinis (JFU)', ''),
(15, 'Kabag Akuntansi', ''),
(16, 'Kepala Bagian Pendidikan dan Penelitian', ''),
(17, 'Perawat (JFU)', ''),
(18, 'Kepala Sub Bagian Tata Usaha dan Rumah Tangga', ''),
(19, 'Dokter Gigi (JFU)', ''),
(20, 'Auditor (JFU)', ''),
(21, 'Fisioterapis Madya (JFT)', ''),
(22, 'Kepala Seksi Pelayanan Medik Rawat Jalan', ''),
(23, 'Perencana (JFU)', ''),
(24, 'Pengevaluasi (JFU)', ''),
(25, 'Perawat Penyelia (JFT)', ''),
(26, 'Auditor Muda (JFT)', ''),
(27, 'Dokter Muda (JFT)', ''),
(28, 'Kepala Sub Bagian Akuntansi Manajemen dan Verifikasi', ''),
(29, 'Teknisi Elektromedis Penyelia (JFT)', ''),
(30, 'Penyusun Bahan Pemeriksaan (JFU)', ''),
(31, 'Pranata Laboratorium Kesehatan Penyelia (JFT)', ''),
(32, 'Perekam Medis Penyelia (JFT)', ''),
(33, 'Kepala Sub Bagian Mobilisasi Dana', ''),
(34, 'Fisioterapis Penyelia (JFT)', ''),
(35, 'Pengelola Pengadaan Barang /Jasa (JFU)', ''),
(36, 'Radiografer Penyelia (JFT)', ''),
(37, 'Kepala Sub Bagian Kepegawaian', ''),
(38, 'Perawat Muda (JFT)', ''),
(39, 'Kepala Sub Bagian Perencanaan dan Evaluasi', ''),
(40, 'Dokter Pendidik Klinis Muda (JFT)', ''),
(41, 'Pekerja Sosial Penyelia (JFT)', ''),
(42, 'Kepala Seksi Pelayanan Keperawatan Rawat Inap', ''),
(43, 'Pengolah Data (JFU)', ''),
(44, 'Kepala Subbagian Hukum, Organisasi dan Pemasaran', ''),
(45, 'Kasubbag Perbendaharaan', ''),
(46, 'Pustakawan (JFU)', ''),
(47, 'Pengadministrasi Umum (JFU)', ''),
(48, 'Fisioterapis Muda (JFT)', ''),
(49, 'Kepala Bidang Pelayanan Keperawatan', ''),
(50, 'Pranata Laboratorium Kesehatan Muda (JFT)', ''),
(51, 'Sanitarian Muda (JFT)', ''),
(52, 'Nutrisionis Muda (JFT)', ''),
(53, 'Ortotis Prostetis Penyelia (JFT)', ''),
(54, 'Kepala Seksi Pelayanan Rawat Inap', ''),
(55, 'Dokter (JFU)', ''),
(56, 'Sanitarian (JFU)', ''),
(57, 'Dokter Pertama (JFT)', ''),
(58, 'Analis Kepegawaian Penyelia (JFT)', ''),
(59, 'Kepala Seksi Pelayanan Keperawatan Rawat Jalan', ''),
(60, 'Sanitarian Penyelia (JFT)', ''),
(61, 'Nutrisionis Penyelia (JFT)', ''),
(62, 'Kepala Sub Bagian Pendidikan dan Pelatihan Kesehatan', ''),
(63, 'Bendahara (JFU)', ''),
(64, 'Radiografer Pelaksana Lanjutan (JFT)', ''),
(65, 'Penata Laporan Keuangan (JFU)', ''),
(66, 'Pranata Laboratorium Kesehatan Pelaksana Lanjutan (JFT)', ''),
(67, 'Kepala Sub Bagian Akuntansi Keuangan', ''),
(68, 'Kasubbag Penyusunan Anggaran', ''),
(69, 'Pranata Komputer (JFU)', ''),
(70, 'Analis Kepegawaian (JFU)', ''),
(71, 'Perawat Gigi Penyelia (JFT)', ''),
(72, 'Asisten Apoteker Penyelia (JFT)', ''),
(73, 'Perawat Pelaksana Lanjutan (JFT)', ''),
(74, 'Okupasi Terapis Penyelia (JFT)', ''),
(75, 'Teknisi Jaringan (Air, Listrik, Telp) (JFU)', ''),
(76, 'Pembantu Orang Sakit (JFU)', ''),
(77, 'Petugas Gudang (JFU)', ''),
(78, 'Pranata Hubungan Masyarakat Pelaksana Lanjutan (JFT)', ''),
(79, 'Binatu (JFU)', ''),
(80, 'Pranata Hubungan Masyarakat Pertama (JFT)', ''),
(81, 'Analis Kepegawaian Pelaksana Lanjutan (JFT)', ''),
(82, 'Pengadministrasi Keuangan (JFU)', ''),
(83, 'Bendahara Pembantu/PUM (JFU)', ''),
(84, 'Pengelola BMN (JFU)', ''),
(85, 'Perawat Pertama (JFT)', ''),
(86, 'Fisioterapis Pelaksana Lanjutan (JFT)', ''),
(87, 'Operator Mesin (Lift, Genset, Air) (JFU)', ''),
(88, 'Fisioterapis Pertama (JFT)', ''),
(89, 'Nutrisionis Pertama (JFT)', ''),
(90, 'Pembuat Daftar Gaji (JFU)', ''),
(91, 'Teknisi Elektromedis Pelaksana Lanjutan (JFT)', ''),
(92, 'Asisten Apoteker Pelaksana Lanjutan (JFT)', ''),
(93, 'Arsiparis (JFU)', ''),
(94, 'Juru Bayar/Kasir (JFU)', ''),
(95, 'Perekam Medis Pelaksana Lanjutan (JFT)', ''),
(96, 'Apoteker (JFU)', ''),
(97, 'Perawat Gigi Pelaksana Lanjutan (JFT)', ''),
(98, 'Perekam Medis Pertama (JFT)', ''),
(99, 'Teknisi Gigi Pelaksana Lanjutan (JFT)', ''),
(100, 'Pekerja Sosial Pertama (JFT)', ''),
(101, 'Pengemudi Ambulan (JFU)', ''),
(102, 'Teknisi Mesin (JFU)', ''),
(103, 'Okupasi Terapis Pelaksana Lanjutan (JFT)', ''),
(104, 'Verifikator Keuangan (JFU)', ''),
(105, 'Nutrisionis Pelaksana Lanjutan (JFT)', ''),
(106, 'Ortotis Prostetis Pelaksana Lanjutan (JFT)', ''),
(107, 'Pramu (JFU)', ''),
(108, 'Terapis Wicara Pelaksana Lanjutan (JFT)', ''),
(109, 'Perekam Medis Pelaksana (JFT)', ''),
(110, 'Ortotis Prostetis Pelaksana (JFT)', ''),
(111, 'Perawat Pelaksana (JFT)', ''),
(112, 'Fisioterapis Pelaksana (JFT)', ''),
(113, 'Pranata Laboratorium Kesehatan Pelaksana (JFT)', ''),
(114, 'Radiografer Pelaksana (JFT)', ''),
(115, 'Asisten Apoteker Pelaksana (JFT)', ''),
(116, 'Pengelola Anggaran (JFU)', ''),
(117, 'Analis LHP (JFU)', ''),
(118, 'Caraka (JFU)', ''),
(119, 'Perawat Gigi Pelaksana (JFT)', ''),
(120, 'Ortotik Prostetis Pemula (JFU)', ''),
(121, 'Perawat Pemula (JFU)', ''),
(122, 'Asisten Apoteker Pemula (JFU)', ''),
(123, 'Perekam Medis Pemula (JFU)', ''),
(124, 'Petugas Keamanan (JFU)', ''),
(125, 'Pranata Hubungan Masyarakat Pemula (JFU)', ''),
(126, 'Kepala Sub Bagian Pendidikan dan Penelitian Non Kesehatan', ''),
(127, 'Fisioterapis (JFU)', ''),
(128, 'Radiografer Pemula (JFU)', ''),
(129, 'Asisten Apoteker', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `refjenjang`
--

CREATE TABLE `refjenjang` (
  `id_jenjang` smallint(3) NOT NULL,
  `nm_jenjang` varchar(50) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `refmenu`
--

CREATE TABLE `refmenu` (
  `idmenu` int(3) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `sub` tinyint(4) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `refmenu`
--

INSERT INTO `refmenu` (`idmenu`, `menu`, `link`, `icon`, `sub`, `active`) VALUES
(1, 'Pengusulan Pelatihan', 'usulan', 'fa fa-share-square-o', 10, 1),
(2, 'Rekap Pelatihan', 'datausulan', 'fa fa-list-alt\n', 10, 1),
(3, 'Persetujuan Pelatihan', 'persetujuan', 'fa fa-thumbs-up', 10, 1),
(4, 'Rencana Pelatihan', 'rencana', 'fa fa-calendar', 10, 1),
(5, 'Realisasi Pelatihan', 'realisasi', 'fa fa-flag', 10, 1),
(7, 'Rekap Usulan Pelatihan', 'laporan/laprekap', '', 16, 1),
(8, 'Rekap Jadwal Pelatihan', 'laporan/lapjadwal', '', 16, 1),
(9, 'Resume Pelatihan', 'laporan/lapresume', '', 16, 1),
(10, 'Pelatihan', '', 'fa fa-laptop', 0, 1),
(11, 'Pendidikan', 'pendidikan', 'fa  fa-graduation-cap', 0, 1),
(12, 'Penelitian', 'penelitian', 'fa fa-book', 0, 1),
(16, 'Laporan', '', 'fa fa-file-text', 0, 1),
(20, 'Setting', '', 'fa fa-cogs', 0, 1),
(21, 'Pengguna', 'setting/user', '', 20, 1),
(22, 'Periode Usulan', 'setting/onoff', '', 20, 1),
(23, 'Satker', 'setting/satker', '', 20, 1),
(24, 'Ref. Universitas', 'setting/refuniv', '', 20, 1),
(25, 'Ref. Jenjang Pend.', 'setting/refjenjang', '', 20, 1),
(26, 'Pegawai', 'setting/pegawai', '', 20, 1),
(27, 'Resume SILAT', 'laporan/silatres', '', 16, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `refpegawai`
--

CREATE TABLE `refpegawai` (
  `id_pegawai` int(4) NOT NULL,
  `badge` varchar(5) DEFAULT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `nip_lama` varchar(100) DEFAULT NULL,
  `no_kartu_pegawai` varchar(100) DEFAULT NULL,
  `nama_pegawai` varchar(150) DEFAULT NULL,
  `tempat_lahir` varchar(150) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `didik_id` smallint(3) NOT NULL,
  `status_pegawai` varchar(50) DEFAULT NULL,
  `alamat` text,
  `status_pegawai_pangkat` varchar(50) DEFAULT NULL,
  `id_golongan` int(4) DEFAULT NULL,
  `id_status_jabatan` int(4) DEFAULT NULL,
  `id_jabatan` int(4) DEFAULT NULL,
  `id_unit_kerja` int(4) DEFAULT NULL,
  `id_satuan_kerja` int(4) DEFAULT NULL,
  `lokasi_kerja` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `refsatker`
--

CREATE TABLE `refsatker` (
  `id_satuan_kerja` int(4) NOT NULL,
  `id_dept` int(1) DEFAULT NULL,
  `id_unit` int(4) DEFAULT NULL,
  `nama_satuan_kerja` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `refstatjab`
--

CREATE TABLE `refstatjab` (
  `id_status_jabatan` int(50) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `refstatjab`
--

INSERT INTO `refstatjab` (`id_status_jabatan`, `nama_jabatan`) VALUES
(2, 'STRUKTURAL'),
(3, 'DPK'),
(4, 'DPB'),
(5, 'DITUGASKAN'),
(6, 'FUNGSIONAL'),
(7, '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `refstatpeg`
--

CREATE TABLE `refstatpeg` (
  `id_status_pegawai` int(50) NOT NULL,
  `nama_status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `refstatpeg`
--

INSERT INTO `refstatpeg` (`id_status_pegawai`, `nama_status`) VALUES
(1, 'MENINGGAL DUNIA'),
(2, 'THL'),
(3, 'BLU NON PNS'),
(4, 'CPNS PUSAT'),
(5, 'PNS PUSAT'),
(6, 'OUTSOURCING'),
(9, 'PENSIUN'),
(10, 'BERHENTI/PINDAH'),
(12, 'PPDS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `refstatus`
--

CREATE TABLE `refstatus` (
  `idstatus` tinyint(4) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `refstatus`
--

INSERT INTO `refstatus` (`idstatus`, `status`) VALUES
(0, 'Editing'),
(1, 'Diusulkan'),
(2, 'Diproses'),
(3, 'Disetujui'),
(4, 'Direncanakan'),
(5, 'Terlaksana'),
(6, 'Ditolak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `refunitkerja`
--

CREATE TABLE `refunitkerja` (
  `id_unit_kerja` int(4) NOT NULL,
  `nama_unit_kerja` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `refunitkerja`
--

INSERT INTO `refunitkerja` (`id_unit_kerja`, `nama_unit_kerja`) VALUES
(11, 'BAGIAN PENDIDIKAN DAN PENELITIAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `refuniv`
--

CREATE TABLE `refuniv` (
  `id_univ` int(11) NOT NULL,
  `nm_univ` varchar(100) NOT NULL,
  `lok_univ` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `takses`
--

CREATE TABLE `takses` (
  `id` int(11) NOT NULL,
  `idmenu` int(6) NOT NULL,
  `iduser` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `takses`
--

INSERT INTO `takses` (`id`, `idmenu`, `iduser`, `active`) VALUES
(63, 1, 1, 0),
(64, 2, 1, 1),
(65, 3, 1, 1),
(66, 4, 1, 1),
(67, 5, 1, 1),
(68, 16, 1, 1),
(69, 7, 1, 1),
(70, 8, 1, 1),
(71, 9, 1, 1),
(72, 20, 1, 1),
(73, 21, 1, 1),
(74, 22, 1, 1),
(75, 23, 1, 1),
(76, 24, 1, 1),
(77, 10, 1, 1),
(78, 11, 1, 1),
(79, 12, 1, 1),
(80, 25, 1, 1),
(81, 26, 1, 1),
(151, 27, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `thistori`
--

CREATE TABLE `thistori` (
  `idhistori` int(11) NOT NULL,
  `idplt` int(11) NOT NULL,
  `idstatus` tinyint(4) NOT NULL,
  `tgl` datetime DEFAULT NULL,
  `ket` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `thistori`
--
DELIMITER $$
CREATE TRIGGER `update_status` AFTER INSERT ON `thistori` FOR EACH ROW BEGIN
SET @IDPLT = NEW.IDPLT;
SET @IDSTATUS = NEW.IDSTATUS; 
UPDATE tusulan SET idstatus = @IDSTATUS WHERE idplt = @IDPLT;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tjadwal`
--

CREATE TABLE `tjadwal` (
  `idplt` int(11) NOT NULL,
  `idjenis` tinyint(1) NOT NULL COMMENT '1=Rencana, 2=Realisasi',
  `mulai` date NOT NULL,
  `selesai` date NOT NULL,
  `tempat` varchar(150) NOT NULL,
  `jpl` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Trigger `tjadwal`
--
DELIMITER $$
CREATE TRIGGER `upd_status` AFTER INSERT ON `tjadwal` FOR EACH ROW BEGIN
SET @idplt = new.idplt;
SET @idjenis = new.idjenis;
IF (@idjenis = 1) THEN
   INSERT INTO thistori (idplt, idstatus) VALUES (@idplt, 4);
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tonoff`
--

CREATE TABLE `tonoff` (
  `id` smallint(6) NOT NULL,
  `onoff` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tonoff`
--

INSERT INTO `tonoff` (`id`, `onoff`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpendidikan`
--

CREATE TABLE `tpendidikan` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(4) NOT NULL,
  `jenjang_id` smallint(3) NOT NULL,
  `tipe` tinyint(1) NOT NULL,
  `univ_id` int(11) NOT NULL,
  `fak` varchar(100) NOT NULL,
  `thn` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpenelitian`
--

CREATE TABLE `tpenelitian` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(4) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `data` varchar(100) NOT NULL,
  `thn` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpeserta`
--

CREATE TABLE `tpeserta` (
  `idpeserta` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `idunit` int(4) NOT NULL,
  `idplt` int(11) NOT NULL,
  `susulan` tinyint(1) NOT NULL DEFAULT '0',
  `sesuai` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=tidak mengikuti; 1=mengikuti'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `tpeserta`
--
DELIMITER $$
CREATE TRIGGER `min_pst` AFTER DELETE ON `tpeserta` FOR EACH ROW BEGIN
SET @idplt = old.idplt;
UPDATE tusulan SET jmlpeserta = jmlpeserta-1 WHERE idplt=@idplt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `plus_pst` AFTER INSERT ON `tpeserta` FOR EACH ROW BEGIN
SET @idplt = new.idplt;
UPDATE tusulan SET jmlpeserta = jmlpeserta+1 WHERE idplt=@idplt;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `upd_statpst` AFTER UPDATE ON `tpeserta` FOR EACH ROW BEGIN
SET @idplt = new.idplt;
SET @jml = (SELECT COUNT(idplt) AS jml from tpeserta 
           WHERE idplt=@idplt and status = 1 LIMIT 1); 
UPDATE tusulan SET jmlpeserta = @jml WHERE idplt=@idplt;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tuser`
--

CREATE TABLE `tuser` (
  `iduser` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `updpass` date DEFAULT NULL,
  `nama` varchar(30) NOT NULL,
  `idsatker` int(4) NOT NULL,
  `idunit` int(4) DEFAULT NULL,
  `level` tinyint(2) NOT NULL COMMENT '0:admin, 1:diklit, 2:user/satker',
  `status` tinyint(1) NOT NULL COMMENT '0:Tidak aktif, 1:Aktif',
  `lastlog` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tuser`
--

INSERT INTO `tuser` (`iduser`, `username`, `password`, `updpass`, `nama`, `idsatker`, `idunit`, `level`, `status`, `lastlog`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2019-09-25', 'Administrator', 0, 11, 0, 1, '2019-09-25 08:42:51');

--
-- Trigger `tuser`
--
DELIMITER $$
CREATE TRIGGER `add_takses` AFTER INSERT ON `tuser` FOR EACH ROW BEGIN SET @iduser = new.iduser; SET @level = new.level; if (@level = 2) THEN INSERT INTO takses(idmenu, iduser, active) VALUES (1, @iduser, 1); end IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tusulan`
--

CREATE TABLE `tusulan` (
  `idplt` int(11) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `id_unit_kerja` int(4) NOT NULL,
  `nmplt` text NOT NULL,
  `uraian` varchar(300) NOT NULL,
  `jmlpeserta` int(3) NOT NULL DEFAULT '0',
  `idstatus` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `refdirektorat`
--
ALTER TABLE `refdirektorat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refjabatan`
--
ALTER TABLE `refjabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `refjenjang`
--
ALTER TABLE `refjenjang`
  ADD PRIMARY KEY (`id_jenjang`);

--
-- Indexes for table `refmenu`
--
ALTER TABLE `refmenu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indexes for table `refpegawai`
--
ALTER TABLE `refpegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD UNIQUE KEY `id_pegawai` (`id_pegawai`) USING BTREE,
  ADD UNIQUE KEY `badge` (`badge`) USING BTREE;

--
-- Indexes for table `refsatker`
--
ALTER TABLE `refsatker`
  ADD PRIMARY KEY (`id_satuan_kerja`),
  ADD UNIQUE KEY `id_satuan_kerja` (`id_satuan_kerja`) USING BTREE;

--
-- Indexes for table `refstatjab`
--
ALTER TABLE `refstatjab`
  ADD PRIMARY KEY (`id_status_jabatan`);

--
-- Indexes for table `refstatpeg`
--
ALTER TABLE `refstatpeg`
  ADD PRIMARY KEY (`id_status_pegawai`);

--
-- Indexes for table `refstatus`
--
ALTER TABLE `refstatus`
  ADD PRIMARY KEY (`idstatus`);

--
-- Indexes for table `refunitkerja`
--
ALTER TABLE `refunitkerja`
  ADD PRIMARY KEY (`id_unit_kerja`);

--
-- Indexes for table `refuniv`
--
ALTER TABLE `refuniv`
  ADD PRIMARY KEY (`id_univ`);

--
-- Indexes for table `takses`
--
ALTER TABLE `takses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idmenu` (`idmenu`,`iduser`);

--
-- Indexes for table `thistori`
--
ALTER TABLE `thistori`
  ADD PRIMARY KEY (`idhistori`);

--
-- Indexes for table `tjadwal`
--
ALTER TABLE `tjadwal`
  ADD PRIMARY KEY (`idplt`,`idjenis`);

--
-- Indexes for table `tonoff`
--
ALTER TABLE `tonoff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tpendidikan`
--
ALTER TABLE `tpendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tpenelitian`
--
ALTER TABLE `tpenelitian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tpeserta`
--
ALTER TABLE `tpeserta`
  ADD PRIMARY KEY (`idpeserta`),
  ADD UNIQUE KEY `id_pegawai` (`id_pegawai`,`idplt`) USING BTREE;

--
-- Indexes for table `tuser`
--
ALTER TABLE `tuser`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `userid` (`username`),
  ADD KEY `idunit` (`idunit`);

--
-- Indexes for table `tusulan`
--
ALTER TABLE `tusulan`
  ADD PRIMARY KEY (`idplt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `refjabatan`
--
ALTER TABLE `refjabatan`
  MODIFY `id_jabatan` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `refjenjang`
--
ALTER TABLE `refjenjang`
  MODIFY `id_jenjang` smallint(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `refmenu`
--
ALTER TABLE `refmenu`
  MODIFY `idmenu` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `refpegawai`
--
ALTER TABLE `refpegawai`
  MODIFY `id_pegawai` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `refsatker`
--
ALTER TABLE `refsatker`
  MODIFY `id_satuan_kerja` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `refstatjab`
--
ALTER TABLE `refstatjab`
  MODIFY `id_status_jabatan` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `refstatpeg`
--
ALTER TABLE `refstatpeg`
  MODIFY `id_status_pegawai` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `refstatus`
--
ALTER TABLE `refstatus`
  MODIFY `idstatus` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `refunitkerja`
--
ALTER TABLE `refunitkerja`
  MODIFY `id_unit_kerja` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `refuniv`
--
ALTER TABLE `refuniv`
  MODIFY `id_univ` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `takses`
--
ALTER TABLE `takses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;
--
-- AUTO_INCREMENT for table `thistori`
--
ALTER TABLE `thistori`
  MODIFY `idhistori` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tpendidikan`
--
ALTER TABLE `tpendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tpenelitian`
--
ALTER TABLE `tpenelitian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tpeserta`
--
ALTER TABLE `tpeserta`
  MODIFY `idpeserta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tuser`
--
ALTER TABLE `tuser`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `tusulan`
--
ALTER TABLE `tusulan`
  MODIFY `idplt` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tuser`
--
ALTER TABLE `tuser`
  ADD CONSTRAINT `idunit` FOREIGN KEY (`idunit`) REFERENCES `refunitkerja` (`id_unit_kerja`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
